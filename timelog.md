# Timelog

* Helping Students practice expression evaluation (ID: 8753)
* Justyna Toporkiewicz
* 2270645T
* Quintin Cutts

## Week 1

### 26 Sep 2019

* *4 hours* Read the project guidance notes
* *0.5 hour* Create a Mind Map and write summer work report
* *1 hour* Created GitLab repository and cloned the cookiecutter for the projects
* *0.5 hour* Preparations for meeting with supervisor
* *1 hour* Research - looking for research papers, blogs and articles about the topic

### 27 Sep 2019

* *1 hour* Supervisor meeting

### 29 Sep 2019

* *0.5 hour* Trying to install baseline app - unsuccessfully
* *1.5 hours* Reading baseline code

## Week 2

### 30 Sep 2019

* *0.5 hour* Setting up code on lab machines
* *0.5 hour* Preparations for meeting with supervisor
* *0.5 hour* Planning the structure of the exercises

### 1 Oct 2019

* *0.5 hour* Supervisor meeting

### 3 Oct 2019

* *2.5 hours* Writing a survey

### 4 Oct 2019

* *1 hour* Trying to troubleshoot the base code to install

### 6 Oct 2019

* *1 hour* Trying to troubleshoot the base code to install

## Week 3

### 7 Oct 2019

* *0.5 hour* Preparations for meeting with supervisor

### 8 Oct 2019

* *0.5 hour* Supervisor meeting

### 10 Oct 2019

* *0.5 hour* Reading base code
* *0.5 hour* Planning code structure

### 12 Oct 2019

* *1.5 hours* Coding - start of components

### 13 Oct 2019

* *0.5 hour* Preparations for meeting with supervisor

## Week 4

### 14 Oct 2019

* *1.5 hours* Coding - working on more basics

### 15 Oct 2019

* *0.5 hour* Supervisor meeting

### 20 Oct 2019

* *1 hour* Coding - showing the components in the app

## Week 5

### 21 Oct 2019

* *0.5 hour* Coding - fixing small errors
* *0.5 hour* Coding - figuring out underline

### 22 Oct 2019

* *1.5 hours* Coding - layout for underline finished

### 24 Oct 2019

* *0.5 hour* Brainstorming

### 27 Oct 2019

* *1.5 hours* Coding - fixing labels and finishing up other components

## Week 6

### 28 Oct 2019

* *1 hour* Coding - start of functionality

### 29 Oct 2019

* *1.5 hours* Coding - clicking tokens creates an underline
* *1.5 hours* Coding - deleting underlines
* *0.5 hour* Supervisor meeting

## Week 7

### 4 Nov 2019

* *3.5 hours* Coding - creating menu

### 5 Nov 2019

* *1.5 hours* Coding - end of part 1
* *1 hour* Putting the documentation on the wiki
* *0.5 hour* Supervisor meeting

### 10 Nov 2019

* *1 hour* Coding - part 2 structure finished

## Week 8

### 11 Nov 2019

* *1.5 hours* Coding - start of solutions

### 12 Nov 2019

* *1.5 hours* Coding - written the checker for part 1 (version 1) and start of exercise creation page
* *0.5 hour* Preparations for meeting with supervisor
* *0.5 hour* Supervisor meeting

### 14 Nov 2019

* *1.5 hours* Coding - trying to figure out adding and removing variables

### 15 Nov 2019

* *0.5 hour* Additional supervisor meeting

### 17 Nov 2019

* *1.5 hours* Coding - creation pages and admin page changes

## Week 9

### 18 Nov 2019

* *0.5 hour* Brainstorming - designs for solutions part 1
* *1.5 hours* Coding - trying to figure out saving the variables creation

### 19 Nov 2019

* *1 hour* Coding - saving the variables finished
* *0.5 hour* Preparations for meeting with supervisor
* *0.5 hour* Supervisor meeting

### 22 Nov 2019

* *2 hours* Coding - fixing bugs in creation and exercise part 2 pages

### 23 Nov 2019

* *0.5 hour* Coding - refactor Underline

## Week 10

### 25 Nov 2019

* *2 hours* Coding - solutions part 2 done + small fix in exercise part 2

### 26 Nov 2019

* *0.5 hour* Brainstorming - outputting parts of state to database
* *0.5 hour* Preparations for meeting with supervisor
* *0.5 hour* Supervisor meeting

### 1 December 2019

* *0.5 hour* Brainstorming - database structure
* *0.5 hour* Brainstorming - testing examples

## Week 11

### 5 December 2019

* *0.5 hour* Supervisior meeting

## Week 12

### 10 December 2019

* *2 hours* Preparation for writing the essay

### 11 December 2019

* *0.5 hour* Coding - preparing the database

### 12 December 2019

* *3.5 hours* Coding - connecting to database

### 13 December 2019

* *2.5 hours* Coding - connecting to database

### 15 December 2019

* *5 hours* Coding - connecting to database

## Week 13

### 16 December 2019

* *1.5 hours* Coding - debugging and clean up
* *0.5 hour* Contacting supervisor

### 17 December 2019

* *1 hour* Supervisor meeting

### 18 December 2019

* *4 hours* Coding - addressing supervisor comments

### 20 December 2019

* *1.5 hours* Coding - addressing comments
* *2 hours* Writing status report

### 21 December 2019

* *0.5 hour* Organisation - tasks requesting system
* *2 hours* Coding - addressing comments

## Week 14

### 25 December 2019

* *0.5 hour* Report - initial setup

## Week 15

### 3 January 2020

* *3 hours* Report - planning

### 4 January 2020

* *4.5 hours* Coding - addressing comments (navigation)

## Week 16

### 8 January 2020

* *1 hour* Communication
* *3.5 hours* Writing - start of requirements section

### 9 January 2020

* *3 hours* Writing - requirements section

### 10 January 2020

* *1.5 hours* Writing - requirements section finished

### 11 January 2020

* *2.5 hours* Writing - design section started

## Week 17

### 14 January 2020

* *0.5 hour* Supervisor meeting prep
* *0.5 hour* Supervisor meeting

### 15 January 2020

* *1 hour* Refactoring code - exercise creation page

### 17 January 2020

* *1.5 hours* Refactoring code - exercise creation page finished

### 18 January 2020

* *0.5 hour* Refactoring code - end
* *2 hours* Redesigning the application

### 19 January 2020

* *3 hours* Preparing testing tools
* *2 hours* Redesigning the application - figuring out how to change the background
* *0.5 hour* Writing more tests

## Week 18

### 21 January 2020

* *4.5 hours* Writing tests - finished exercise creation tests and started exercise rendering tests
* *1 hour* Supervisor (+ other project student) meeting

### 24 January 2020

* *1 hour* Writing tests - adding to exercise creation tests

### 26 January 2020

* *5.5 hours* Writing tests - for individual components making up the exercise structures

## Week 19

### 27 January 2020

* *4 hours* Writing tests - testing the full exercise pages (end of unit testing)

### 28 January 2020

* *1.5 hours* Updating gitlab wiki - putting up notes from supervisor meetings
* *0.5 hour* Supervisor meeting

### 29 January 2020

* *0.5 hour* Communication (with person working on university a server) and documentation

### 31 January 2020

* *1.5 hours* Writing handouts for user testing

### 1 February 2020

* *1.5 hours* Writing handouts for user testing
* *1 hour* Attempting to improve underline value input

### 2 February 2020

* *3 hours* Writing dissertation - Design done

## Week 20

### 3 February 2020

* *1.5 hours* Addressing comments - underline input box height + one solution per exercise per student

### 4 February 2020

* *1 hour* Setup project on Trace
* *1.5 hours* Connecting to master students database system
* *1 hour* Supervisor meeting

### 5 February 2020

* *0.5 hour* Email to supervisor
* *1 hour* Writing dissertation

### 9 February 2020

* *1.5 hours* Update home and login pages
* *1 hours* Fix user testing exercises

## Week 21

### 10 February 2020

* *0.5 hour* Preparing for supervisor meeting - make plan for implementation chapter

### 11 February 2020

* *1 hour* Connecting to master students database system
* *0.5 hour* Updating and fixing lab setup
* *1 hour* Supervisor meeting

### 15 February 2020

* *2 hours* Addressing small comments - student comment boxes + update titles + fix small bug

## Week 22

### 17 February 2020

* *0.5* Planning and brainstorming
* *2 hours* Small code updates + testing - updating the layouts of the components, adding log outs and requiring log in before showing the exercise list

### 18 February 2020

* *0.5 hour* Writing the report - implementation chapter start
* *0.5 hour* Supervisor meeting
* *1 hour* Connecting to the master students database system

### 19 February 2020

* *1.5 hours* Writing the report

### 22 February 2020

* *1 hour* Writing the report - implementation chapter continued

### 23 February 2020

* *4.5 hours* Writing the report - implementation chapter continued

## Week 23

### 24 February 2020

* *0.5 hour* Writing the report
* *2.5 hours* Coding

### 25 February 2020

* *1.5 hours* Connecting to the master students database system
* *1 hour* Meeting with supervisor

### 27 February 2020

* *1.5 hours* Small stylistic changes

### 28 February 2020

* *1 hour* Coding
* *1 hour* Meeting with supervisor
* *2 hours* Writing the report

### 1 March 2020

* *2.5 hour* Updating the database and allowing teachers to see student solutions

## Week 24

### 2 March 2020

* *0.5 hour* Meeting with supervisor - picking evaluation participants
* *1 hour* Contacting evaluation participants

### 3 March 2020

* *1.5 hours* Connecting to the master students database system

### 4 March 2020

* *3 hours* Preparing the evaluation handout
* *6 hours* Preparing the evaluation handout + ethics approval

### 5 March 2020

* *3 hours* Evaluation
* *1.5 hours* Evaluation

### 6 March 2020

* *2.5 hours* Evaluation

### 8 March 2020

* *3.5 hours* Fixing bugs found during evaluation

## Week 25

### 9 March 2020

* *7.5 hours* Evaluation

### 10 March 2020

* *2.5 hours* Evaluation
* *1 hour* Connecting to the master students database system

### 11 March 2020

* *0.5 hour* Preparing for meetings
* *0.5 hour* Meeting with Professor Williamson
* *1 hour* Meeting with supervisor
* *0.5 hour* Preparing for evaluation for the following day

### 12 March 2020

* *3 hours* Professional evaluation
* *1.5 hours* Added functionality to remove abandoned exercises

### 13 March 2020

* *1 hour* Evaluation with 4th years
* *0.5 hour* Updating the removal of previous solutions

### 14 March 2020

* *2.5 hours* Writing dissertation

### 15 March 2020

* *1 hour* Writing dissertation

## Week 26

### 16 March 2020

* *4.5 hours* Writing dissertation
* *1 hour* Evaluation with 4th years
* *0.5 hour* Addressing comments from evaluation

### 17 March 2020

* *2.5 hours* Writing dissertation
* *1 hour* Fixing bugs

### 18 March 2020

* *0.5 hour* Meeting with supervisor (Zoom call)

### 21 March 2020

* *1 hour* Writing dissertation
* *4 hours* Updating tests and refactoring code

### 22 March 2020

* *3 hours* Writing dissertation
* *3 hours* Updating tests and create warning for deleting exercises

## Week 27

### 23 March 2020

* *0.5 hour* Updating style of components
* *4 hours* Writing dissertation

### 25 March 2020

* *0.5 hour* Meeting with supervisor (Zoom call)
* *3.5 hours* Writing dissertation
* *0.5 hour* Updating the home buttons

### 26 March 2020

* *4.5 hours* Writing dissertation

### 27 March 2020

* *2.5 hours* Writing dissertation
* *2 hours* Updating file structure and project documentation

### 28 March 2020

* *3 hours* Writing dissertation

### 29 March 2020

* *5 hours* Writing dissertation

## Week 28

### 30 March 2020

* *7 hours* Writing dissertation

### 31 March 2020

* *6.5 hours* Writing dissertation

### 1 April 2020

* *2.5 hours* Writing dissertation
* *0.5 hours* Supervisor meeting (Zoom)

### 2 April 2020

* *2 hours* Writing dissertation
* *3 hours* Planning presentation

### 3 April 2020

* *3 hours* Re-reading dissertation
* *1.5 hours* Creating presentation slides

### 4 April 2020

* *1 hour* Finishing up presentation
* *1.5 hours* Finishing up appendices
* *1 hour* Cleaning up files
* *0.5 hour* Recording presentation