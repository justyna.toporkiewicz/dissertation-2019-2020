# Summer work

* Helping Students practice expression evaluation (ID: 8753)
* Justyna Toporkiewicz
* 2270645T
* Quintin Cutts

## Research completed

As advised, I have read the provided dissertation, which is a basis for the current project, as well as learned the functionality of React as well as Redux. I have also looked into some documentation on the task online.

## Ideas developed

I am considering splitting the task into multiple parts and allowing for re-usability of components to help with further expansion in the future. Also, as discussed, I am planning on completing the code sections of the project early in the semester to allow plenty of time to test and improve on the features as needed.

## Concerns or risks identified

Main concern is not finding enough testing participants with the appropriate level of knowledge (beginners level students in computer science).