# Readme

This project is an extension to a notional machine which is an educational tool allowing teachers to make exercises which provide immediate feedback to students. The development was mostly carried out in `.\notional-machine\client\src\components` in folders starting with capital letters with additional minor changes to the components in the folder `pages` within the same directory.

The notional machine code is divided into two distinct sections which are the client and the server directory. The server directory is centered around managing the local database through models and API calls. The client directory is centered around the creation and managing the layouts of pages which show up in the client side of the application as well as connecting to the database.

The components created in the client directory are individual elements of the page, split by their use and relation to other components. Components related by their purpose are gathered into directories for organization. Smaller components are then used in bigger components (which become their parents) to create entire pages. The parent components which hold the structure of entire pages intorduced during this project are placed in directories ending with *Pages*. The choice of pages to be rendered is done through the Router within the *App* file.

## Build instructions

This project can be run using two command terminals. In the first window go to the directory `.\notional-machine\client` and in the other go to the directory `.\notional-machine\server`. In both terminals run the commands `npm install` to download all packages necessary for the correct functioning of the system, followed by the command `npm start` to compile the files and enable the use of the system. The application can be then accessed by going to the url `localhost:8080/`.

The application is provided with automated unit tests which can be run by opening a command terminal, proceeding to the directory `.\notional-machine\client` and using the command `npm test`.

The application can also be build using the command `npm run-script build` in the `.\notional-machine\client` directory. The application then provides an *.html* file that can be used to access the tool. However due to the system not being connected to a remote database, the application cannot function fully in this mode.

### Requirements

* Node JS - version 10.x
* Tested on Windows 10 using Firefox, Chrome, Internet Explorer and Safari browsers

### Build steps

Clone the repository.

Open two terminals.

In the first terminal:
1. `cd notional-machine\client`
2. `npm install`
3. `npm start`

In the second terminal: 
1. `cd notional-machine\server`
2. `npm install`
3. `npm start`

### Test steps

The software can be tested in two ways:
* by running automated tests using the commands `cd notional-machine\client` followed by `npm test` within a terminal
* by starting the software using the commands `npm install` and `npm start` in two terminal windows which are within the directories `notional-machine\client` and `notional-machine\server`
