# User manual 

## Building the application

### Requirements

* Node JS - version 10.x
* Tested on Windows 10 using Firefox, Chrome, Internet Explorer and Safari browsers

### Build steps

Clone the repository.

Open two terminals from this location (`src` directory).

In the first terminal:
1. `cd notional-machine\client`
2. `npm install`
3. `npm start`

In the second terminal: 
1. `cd notional-machine\server`
2. `npm install`
3. `npm start`

## Using the application

When the application is running, opening the path `localhost:8080` presents the users with the home page of the application. Continuing through the login page students and admins can access exercise list pages. Through the admin exercise list page, users can access the exercise creation page.

Admin users are presented with exercise pages to provide the first solution to a created exercise and when updating the exercise at any later point. To interact with the exercise, admins can use the features provided in the exercise menu in the admin exercise list page. The exercises can be reviewed and the solutions to them can be updated. Checking the solutions provided by the students can also be reached threough the main admin page. Deleting the exercise presents a warning pop-up.

Students can create a user profile using the sign up page after which they can use those credentials to log in and are presented with the exercise list page. Completing the exercises is accomplished using the same pages that are presented to admins upon exercise creation, however, after each step of the task, students are presented with feedback pages. Upon completing the exercise, the exercise list presented to the student is updated to inform the students that they have attempted the exercise.
