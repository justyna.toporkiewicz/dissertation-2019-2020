const Sequelize = require('sequelize')

const sequelize = new Sequelize('notional-machine-db', null, null, {
  dialect: 'sqlite',
  storage: './notional-machine-db.sqlite'
})

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  })

/** Define all ORM models **/
const Pupil = sequelize.define('pupil', {
  username: { type: Sequelize.STRING },
  password: { type: Sequelize.STRING }
})

const Exercise = sequelize.define('exercise', {
  name: { type: Sequelize.STRING },
  code_fragment: { type: Sequelize.TEXT }
})

const Solution = sequelize.define('solution', {
  exercise_ID: { type: Sequelize.INTEGER },
  pupil_ID: { type: Sequelize.INTEGER },
})

const Step = sequelize.define('step', {
  number: { type: Sequelize.INTEGER },
  output: { type: Sequelize.STRING },
  input: { type: Sequelize.STRING },
  expr_eval: { type: Sequelize.STRING },
  solution_ID: { type: Sequelize.INTEGER },
  arrow_ID: { type: Sequelize.INTEGER }
})

const Arrow = sequelize.define('arrow', {
  start_row: { type: Sequelize.INTEGER },
  end_row: { type: Sequelize.INTEGER },
  annotation: { type: Sequelize.BOOLEAN },
  solution_ID: { type: Sequelize.INTEGER }
})

const Variable = sequelize.define('variable', {
  name: { type: Sequelize.STRING },
  value: { type: Sequelize.STRING },
  step_ID: { type: Sequelize.INTEGER }
})

const Expression = sequelize.define('expression', {
  line: { type: Sequelize.INTEGER },
  start_pos: { type: Sequelize.INTEGER },
  end_pos: { type: Sequelize.INTEGER },
  solution_ID: { type: Sequelize.INTEGER }
})

const ExpressionExercise = sequelize.define('expression_exercise', {
  expression: { type: Sequelize.STRING },
  name: { type: Sequelize.STRING },
  part1_comment: { type: Sequelize.STRING },
  part2_comment: { type: Sequelize.STRING }
})

const ExpressionUnderline = sequelize.define('expression_underline', {
  exercise_id: { type: Sequelize.STRING },
  student_id: { type: Sequelize.INTEGER },
  startIndex: { type: Sequelize.INTEGER},
  endIndex: { type: Sequelize.INTEGER},
  orderNo: { type: Sequelize.INTEGER }
})

const ExpressionUnderlineValues = sequelize.define('expression_underline_values', {
  exercise_id: { type: Sequelize.INTEGER },
  student_id: { type: Sequelize.INTEGER },
  value: { type: Sequelize.STRING },
  orderNo: { type: Sequelize.INTEGER }
})

const ExpressionVariable = sequelize.define('expression_variable', {
  exercise_id: { type: Sequelize.INTEGER },
  variable: { type: Sequelize.STRING },
})

const SolutionComment = sequelize.define('solution_comment', {
  exercise_id: { type: Sequelize.STRING },
  student_id: { type: Sequelize.STRING },
  comment_body: { type: Sequelize.STRING },
  exercise_part: { type: Sequelize.INTEGER }
})

Pupil.sync()
Exercise.sync()
Solution.sync()
Step.sync()
Arrow.sync()
Variable.sync()
Expression.sync()
ExpressionExercise.sync()
ExpressionUnderline.sync()
ExpressionUnderlineValues.sync()
ExpressionVariable.sync()
SolutionComment.sync()

module.exports = {
  Pupil,
  Exercise,
  Solution,
  Step,
  Arrow,
  Variable,
  Expression,
  ExpressionExercise,
  ExpressionUnderline,
  ExpressionUnderlineValues,
  ExpressionVariable,
  SolutionComment
}
