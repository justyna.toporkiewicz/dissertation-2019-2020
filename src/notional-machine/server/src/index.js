const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const restRouter = require('./router')
const cors = require('cors')
const { Pupil, Exercise, Solution, Step, Arrow, Variable, Expression, ExpressionExercise, ExpressionUnderline, ExpressionUnderlineValues, ExpressionVariable, SolutionComment } =
  require('./models')

/** Enable CORS **/
app.use(cors())

/** Global Body parser middleware to convert the req body to a standard JS object **/
app.use(bodyParser.json())

/** RESTful resources **/
app.use('/pupil', restRouter(Pupil))
app.use('/exercise', restRouter(Exercise))
app.use('/solution', restRouter(Solution))
app.use('/step', restRouter(Step))
app.use('/arrow', restRouter(Arrow))
app.use('/variable', restRouter(Variable))
app.use('/expression', restRouter(Expression))
app.use('/expression_exercise', restRouter(ExpressionExercise))
app.use('/expression_underline', restRouter(ExpressionUnderline))
app.use('/expression_underline_values', restRouter(ExpressionUnderlineValues))
app.use('/expression_variable', restRouter(ExpressionVariable))
app.use('/solution_comment', restRouter(SolutionComment))

app.listen(3000, function() {
  console.log('Server is listening on port 3000.')
})
