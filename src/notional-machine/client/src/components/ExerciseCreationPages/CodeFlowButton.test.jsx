import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import CodeFlowButton from './CodeFlowButton'

const mockFunction = jest.fn(() => "CALLED")

describe("CodeFlowButton", () => {
    it("has correct text", async () => {
        const { getByText } = render(<CodeFlowButton />)
        fireEvent.click(getByText("Program Flow"))
        expect(getByText("Program Flow")).toBeTruthy()
    })

    it("is clickable", async () => {
        const { getByText } = render(<CodeFlowButton onClick={mockFunction} />)
        fireEvent.click(getByText("Program Flow"))
        expect(mockFunction.mock.calls.length).toBe(1)
    })
})
