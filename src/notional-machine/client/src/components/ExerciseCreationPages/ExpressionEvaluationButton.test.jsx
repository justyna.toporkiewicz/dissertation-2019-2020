import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import ExpressionEvaluationButton from './ExpressionEvaluationButton'

const mockFunction = jest.fn(() => "CALLED")

describe("ExpressionEvaluationButton", () => {
    it("has correct text", async () => {
        const { getByText } = render(<ExpressionEvaluationButton />)
        fireEvent.click(getByText("Expression Evaluation"))
        expect(getByText("Expression Evaluation")).toBeTruthy()
    })

    it("is clickable", async () => {
        const { getByText } = render(<ExpressionEvaluationButton onClick={mockFunction} />)
        fireEvent.click(getByText("Expression Evaluation"))
        expect(mockFunction.mock.calls.length).toBe(1)
    })
})