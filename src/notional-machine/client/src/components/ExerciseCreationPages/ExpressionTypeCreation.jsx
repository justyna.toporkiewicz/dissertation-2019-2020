import React from 'react'
import Submit from '../ExpressionExerciseComponents/Submit'
import ExerciseNameBox from './Boxes/ExerciseNameBox'
import ExpressionBox from './Boxes/ExpressionBox'
import VariableInputBox from './Boxes/VariableInputBox'
import NoVariablesBox from './Boxes/NoVariablesBox'
import CommentBox from './Boxes/CommentBox'
import './ExerciseType.css'
import client from '../api-client'
import * as Constant from '../Constants'

function expressionClean(expression) {
    return expression.replace(/\s+/g,' ').trim()
}

class ExpressionTypeCreation extends React.Component {
    constructor() {
      super();
      this.state = {
            name: "",
            expression: "",
            variableNames: [],
            variables: {},
            part1Comment: "",
            part2Comment: "",
        }
      this.onSubmit = this.handleCreateExercise.bind(this)
    }

    updateItem (e) {
        this.setState({[e.target.name]: e.target.value})
    }

    createFirstVariable () {
        this.setState({
            variables: {"variable0": ""},
            variableNames: ["variable0"]
            })
    }

    createVariable () {
        this.setState({
            variables: Object.assign({}, this.state.variables, {["variable"+this.state.variableNames.length]: ""}),
            variableNames: this.state.variableNames.concat(["variable"+this.state.variableNames.length])
            })
    }

    updateVariables (e) {
        this.setState({
            variables: Object.assign({}, this.state.variables, {[e.target.name]: e.target.value})
        })
    }

    deleteVariable (valueName) {
        const copyVariables = this.state.variables
        delete copyVariables[valueName]

        if(valueName < "variable" + (this.state.variableNames.length-1)) {
            for (var key = 0; key < this.state.variableNames.length; key++){
                if("variable" + key > valueName){copyVariables["variable" + (key-1)] = copyVariables["variable"+key]}
            }
            delete copyVariables["variable"+(this.state.variableNames.length-1)]
        }

        this.setState({
            variableNames: this.state.variableNames.filter((input)=> input != "variable" + (this.state.variableNames.length-1)),
            variables: copyVariables
        })
    }

    async handleCreateExercise (e) {
        e.preventDefault()
        if(this.state.name == "" || this.state.expression == "") {
            return null
        }

        const exercise_id = (await client.resource('expression_exercise').create({
            name: this.state.name,
            expression: expressionClean(this.state.expression),
            part1_comment: this.state.part1Comment,
            part2_comment: this.state.part2Comment
        })).id

        for (let variable = 0; variable < this.state.variableNames.length; variable++) {
            const tempVar = this.state.variables[this.state.variableNames[variable]]
            if(tempVar == "") continue
            await client.resource('expression_variable').create({exercise_id: exercise_id, variable: tempVar })
        }

        sessionStorage.setItem(Constant.MODE, Constant.TEACHER_CREATE)
        this.props.history.push('/expression-exercise/'+exercise_id+'/1')
    }

    render () {
        return (
            <form className="centerContent" onSubmit={this.onSubmit}>
                <div className="title">
                    <div className="btn btn-danger smallMargin" onClick={() => this.props.history.push('/admin/exercise-list')}>Go back to admin page</div>
                    <h2>Create Evaluation Type Exercise</h2>
                    <p>Provide the expression and any variables required to calculate the final result</p>
                    <p>You can also leave comments to help students or give them additional instructions</p>
                </div>
                <ExerciseNameBox updateItem={this.updateItem.bind(this)} />
                <ExpressionBox updateItem={this.updateItem.bind(this)} />
                <div className="main">
                    <div className="creationBox body">
                        <p><b>Variables</b> (optional)</p>
                        <p><i>form: name = value</i></p>
                        {this.state.variableNames.length != 0 ?
                            this.state.variableNames.map((valueName, index) => (
                                <VariableInputBox
                                    deleteVariable={this.deleteVariable.bind(this)}
                                    updateVariables={this.updateVariables.bind(this)}
                                    createVariable={this.createVariable.bind(this)}
                                    index={index}
                                    valueName={valueName}
                                    variable={this.state.variables[valueName]}
                                    key={index}
                                />
                            ))
                            :
                            <NoVariablesBox createFirstVariable={this.createFirstVariable.bind(this)} />
                        }
                    </div>
                </div>
                <div className="main">
                    <CommentBox
                        subtitle="Underline Sub-expressions"
                        comment={this.state.part1Comment}
                        variablesCount={this.state.variableNames.length}
                        commentNumber={1}
                        updateItem={this.updateItem.bind(this)}
                    />
                    <CommentBox
                        subtitle="Evaluate Sub-expressions"
                        comment={this.state.part2Comment}
                        variablesCount={this.state.variableNames.length}
                        commentNumber={2}
                        updateItem={this.updateItem.bind(this)}
                    />
                </div>
                <Submit value="Submit and Provide a Solution" />
            </form>
        )
    }
}

export default ExpressionTypeCreation
