import React from 'react'
import Submit from '../ExpressionExerciseComponents/Submit'
import './ExerciseType.css'
import client from '../api-client'


class FlowTypeCreation extends React.Component {
    constructor() {
        super();
        this.state = {value: {name: "", code_fragment: ""}}
        this.onChange = this.handleChange.bind(this)
        this.onSubmit = this.handleCreate.bind(this)
    }

    handleChange (e) {
        this.setState({value: Object.assign({}, this.state.value, { [e.target.name]: e.target.value})})
    }
    
    /** Saves the code fragment **/
    async handleCreate (e) {
        e.preventDefault()
        try {
        const exercise = await client.resource('exercise').create(this.state.value)
        this.props.history.push(`/exercise/${exercise.id}/task/1/admin`)
        } catch (error) {
        console.log(error)
        this.props.hitory.push('/error')
        }
    }

    render(){
        return (
            <form className="centerContent" onSubmit={this.onSubmit}>
                <div className="title">
                    <h1>Create Flow Type Exercise</h1>
                    <p>Provide the code for the exercise in any language</p>
                </div>
                <div className="creationBox body">
                    <p>Name</p>
                    <input name="name" type="text" value={this.state.value.name} onChange={this.onChange} />
                </div>
                <div className="creationBox body">
                    <p>Program Code</p>
                    <textarea rows="6" name="code_fragment" value={this.state.value.code_fragment} onChange={this.onChange} />
                </div>
                <Submit value="Provide Solution" />
            </form>
        )
    }
}

export default FlowTypeCreation