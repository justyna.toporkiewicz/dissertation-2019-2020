import React from "react"
import "./ChoiceButton.css"

const ExpressionEvaluationButton = ({onClick}) => (
    <div className="choiceButton btn btn-primary" onClick={() => onClick()}>
        <b>Expression Evaluation</b>
    </div>
)

ExpressionEvaluationButton.defaultProps = {
    onClick: () => ({})
}

ExpressionEvaluationButton.propTypes = {
    onClick: function(){}
}

export default ExpressionEvaluationButton