import React from 'react'
import { render } from '@testing-library/react'
import CommentBox from './CommentBox'

describe("CommentBox", () => {
    it("renders with the correct number", async () => {
        const { getByText } = render(
            <CommentBox
                comment=""
                variablesCount={0}
                commentNumber={1}
                updateItem={() => ({})}
            />)
        expect(getByText("Part 1 Comments")).toBeTruthy()
    })

    it("has correct value", async () => {
        const { getByText } = render(
            <CommentBox
                comment="test comment"
                variablesCount={0}
                commentNumber={2}
                updateItem={() => ({})}
            />)
        expect(getByText("Part 2 Comments")).toBeTruthy()
        expect(getByText("test comment")).toBeTruthy()
    })
})