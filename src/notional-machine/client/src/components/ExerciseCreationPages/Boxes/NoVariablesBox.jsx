import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

const NoVariablesBox = ({createFirstVariable}) => (
    <div
        onClick={() => createFirstVariable()}
        tabIndex="3"
        onKeyPress={() => createFirstVariable()}
        className="btn btn-sm btn-outline-success textAreaWidth"
    >
        Add a variable
    </div>
)

NoVariablesBox.propTypes = {
    createFirstVariable: PropTypes.func
}

export default NoVariablesBox