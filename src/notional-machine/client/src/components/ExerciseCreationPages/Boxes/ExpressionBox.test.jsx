import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import ExpressionBox from './ExpressionBox'

describe("ExpressionBox", () => {
    it("updates the expression value", async () => {
        const { getByLabelText } = render(<ExpressionBox />)
        
        fireEvent.change(getByLabelText('expression'), {target: {value: '1 + 2'}})

        expect(getByLabelText('expression').value).toBe('1 + 2')
    })
})