import React from 'react'
import PropTypes from 'prop-types'
import '../ExerciseType.css'

const ExpressionBox = ({updateItem}) => (
    <div className="main">
    <div className="creationBox body">
        <p><b>Expression</b></p>
        <input
            className="monospace"
            type="text"
            name="expression"
            size="40"
            onChange={updateItem}
            tabIndex="2"
            aria-label="expression"
        />
    </div>
</div>
)

ExpressionBox.propTypes = {
    updateItem: PropTypes.func
}

export default ExpressionBox