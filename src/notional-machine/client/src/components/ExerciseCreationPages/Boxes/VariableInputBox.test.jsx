import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import VariableInputBox from './VariableInputBox'

const mockDelete = jest.fn(() => "DELETE")
const mockCreate = jest.fn(() => "CREATE")

describe("VariableInputBox", () => {
    it("responds to key presses", async () => {
        const { getByText } = render(
            <VariableInputBox
                deleteVariable={mockDelete}
                createVariable={mockCreate}
            />)
        fireEvent.keyPress(getByText("+"), { key: 'Enter', keyCode: 13 })
        expect(mockCreate.mock.calls.length).toBe(1)

        fireEvent.keyPress(getByText("-"), { key: 'Enter', keyCode: 13 })
        expect(mockDelete.mock.calls.length).toBe(1)
    })
})