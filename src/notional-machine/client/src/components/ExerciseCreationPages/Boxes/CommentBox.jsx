import React from 'react'
import PropTypes from 'prop-types'
import '../ExerciseType.css'

const CommentBox = ({subtitle, comment, variablesCount, commentNumber, updateItem}) => (
    <div className={"creationBox " + (commentNumber == 1 ? "side":"bodyInLine")}>
        <p><b>Part {commentNumber} Comments</b> (optional)</p>
        <p><i>{subtitle}</i></p>
        <p>eg. tips and things to remember</p>
        <textarea
            rows="4"
            name={"part"+commentNumber+"Comment"}
            value={comment}
            onChange={updateItem}
            className="textAreaWidth"
            tabIndex={commentNumber == 1 ? (
                variablesCount == 0 ?
                    "4" :
                    ""+(3+3*variablesCount)
            ) : (
                variablesCount == 0 ?
                    "5" :
                    ""+(4+3*variablesCount)
            )
                }
        />
    </div>
)

CommentBox.propTypes = {
    subtitle: PropTypes.string,
    comment: PropTypes.string,
    variablesCount: PropTypes.number,
    commentNumber: PropTypes.number,
    updateItem: PropTypes.func
}

export default CommentBox