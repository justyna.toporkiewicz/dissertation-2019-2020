import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import './VariableCreationBoxes.css'

const VariableInputBox = ({deleteVariable, updateVariables, createVariable, index, valueName, variable}) => (
    <p>
        <span
            onClick={() => deleteVariable(valueName)}
            tabIndex={""+(3+3*index)}
            onKeyPress={() => deleteVariable(valueName)}
            className="btn btn-xs btn-outline-danger"> -</span>
        <textarea 
            type="text" 
            value={variable} 
            name={valueName} 
            onChange={updateVariables}
            tabIndex={""+(4+3*index)}
            aria-label={"variable"+index}
            className="textAreaWidth monospace"
        />
        <span
            onClick={() => createVariable()}
            tabIndex={""+(5+3*index)}
            onKeyPress={() => createVariable()}
            className="btn btn-xs btn-outline-success">+ </span>
    </p>
)

VariableInputBox.propTypes = {
    deleteVariable: PropTypes.func,
    updateVariables: PropTypes.func,
    createVariable: PropTypes.func,
    index: PropTypes.number,
    valueName: PropTypes.string,
    variable: PropTypes.string
}

export default VariableInputBox