import React from 'react'
import PropTypes from 'prop-types'
import '../ExerciseType.css'

const ExerciseNameBox = ({updateItem}) => (
    <div className="main">
        <div className="creationBox body">
            <p><b>Exercise name</b></p>
            <input
                className="monospace"
                type="text"
                name="name"
                size="40"
                onChange={updateItem}
                aria-label="name"
                tabIndex="1"
            />
        </div>
    </div>
)

ExerciseNameBox.propTypes = {
    updateItem: PropTypes.func
}

export default ExerciseNameBox