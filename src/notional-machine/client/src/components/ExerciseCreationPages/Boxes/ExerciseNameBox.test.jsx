import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import ExerciseNameBox from './ExerciseNameBox'

describe("ExerciseNameBox", () => {
    it("updates the name value", async () => {
        const { getByLabelText } = render(<ExerciseNameBox />)
        
        fireEvent.change(getByLabelText('name'), {target: {value: 'test'}})

        expect(getByLabelText('name').value).toBe('test')
    })
})