import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import NoVariablesBox from './NoVariablesBox'

const tempFunction = jest.fn(() => "CALLED")

describe("NoVariablesBox", () => {
    it("responds to key inputs", async () => {
        const { getByText } = render(<NoVariablesBox createFirstVariable={tempFunction} />)
        fireEvent.keyPress(getByText("Add a variable"), { key: 'Enter', keyCode: 13 })
        expect(tempFunction.mock.calls.length).toBe(1)
    })
})