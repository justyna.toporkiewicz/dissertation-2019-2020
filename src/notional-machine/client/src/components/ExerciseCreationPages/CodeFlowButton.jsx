import React from 'react'
import "./ChoiceButton.css"

const CodeFlowButton = ({onClick}) => (
    <div className="choiceButton btn btn-primary" onClick={() => onClick()}>
        <b>Program Flow</b>
    </div>
)

CodeFlowButton.defaultProps = {
    onClick: () => ({})
}

CodeFlowButton.propTypes = {
    onClick: function(){}
}

export default CodeFlowButton