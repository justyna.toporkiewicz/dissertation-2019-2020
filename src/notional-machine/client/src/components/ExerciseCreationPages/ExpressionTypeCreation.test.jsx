import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import ExpressionTypeCreation from './ExpressionTypeCreation'
import client from '../api-client'
import waitForExpect from 'wait-for-expect'

jest.mock('../api-client')

describe("ExpressionTypeCreation", () => {

    beforeEach(() => {
        Object.defineProperty(window, 'sessionStorage', {
          value: {
            getItem: jest.fn(key => mockStorage[key]),
            setItem: jest.fn((mode, teacher) => (mode, teacher)),
          },
          writable: true
        })
      });
    
    client.resource.mockImplementation((name)=>({create: () => ({id:1})}))
    const historyMock = { push: jest.fn() }


    it("renders all correct elements", async () => {
        const { getByText } = render(<ExpressionTypeCreation />)
        expect(getByText("Create Evaluation Type Exercise")).toBeTruthy()
        expect(getByText("Exercise name")).toBeTruthy()
        expect(getByText("Expression")).toBeTruthy()
        expect(getByText("Variables")).toBeTruthy()
        expect(getByText("Part 1 Comments")).toBeTruthy()
        expect(getByText("Part 2 Comments")).toBeTruthy()
        expect(getByText("Submit and Provide a Solution")).toBeTruthy()
        expect(getByText("Add a variable")).toBeTruthy()
        expect(getByText("Go back to admin page")).toBeTruthy()
    })

    it("clicking the admin page button allows for going back to the previous page", () => {
        const historySpy = jest.spyOn(historyMock, 'push')
        const { getByText } = render(<ExpressionTypeCreation history={historyMock}/>)
        expect(getByText("Create Evaluation Type Exercise")).toBeTruthy()
        fireEvent.click(getByText("Go back to admin page"))
        expect(historySpy).toBeCalled()
    })

    it("replaces the no variables option with an input box when clicked", async () => {
        const { getByText, queryByText } = render(<ExpressionTypeCreation />)
        fireEvent.click(getByText("Add a variable"))
        expect(queryByText("Add a variable")).toBeFalsy()
    })

    it("add more variables and remove them", async () => {
        const { getByText, getAllByText, queryAllByText } = render(<ExpressionTypeCreation />)
        fireEvent.click(getByText("Add a variable"))
        fireEvent.click(getByText("+"))
        expect(getAllByText("+").length).toBe(2)

        fireEvent.click(queryAllByText("-")[0])
        expect(queryAllByText("-").length).toBe(1)

        fireEvent.click(getByText("-"))
        expect(getByText("Add a variable")).toBeTruthy()
    })

    it("updates the variable value", async () => {
        const { getByLabelText, getByText} = render(<ExpressionTypeCreation />)
        fireEvent.click(getByText("Add a variable"))

        fireEvent.change(getByLabelText('variable0'), {target: {value: 'a = 1'}})

        expect(getByLabelText('variable0').value).toBe('a = 1')
    })


    it("not submit an empty exercise", async () => {
        const { getByText } = render(<ExpressionTypeCreation />)
        fireEvent.click(getByText("Submit and Provide a Solution"))
        expect(window.sessionStorage.setItem).toHaveBeenCalledTimes(0)
    })

    it("submits an exercise when name and expression is given", async () => {
        const { getByText, getByLabelText } = render(<ExpressionTypeCreation history={historyMock} />)
        fireEvent.change(getByLabelText('name'), {target: {value: 'test'}})
        fireEvent.change(getByLabelText('expression'), {target: {value: '1 + 2'}})
        
        fireEvent.click(getByText("Submit and Provide a Solution"))
        await waitForExpect(() => expect(window.sessionStorage.setItem).toHaveBeenCalledTimes(1))
    })

    it("submits variables as appropriate", async () => {
        const { getByLabelText, getByText, queryAllByText } = render(<ExpressionTypeCreation history={historyMock} />)
        fireEvent.change(getByLabelText('name'), {target: {value: 'test'}})
        fireEvent.change(getByLabelText('expression'), {target: {value: '1 + 2'}})
        
        fireEvent.click(getByText("Add a variable"))

        fireEvent.change(getByLabelText('variable0'), {target: {value: 'a = 1'}})
        
        fireEvent.click(getByText("+"))
        fireEvent.change(getByLabelText('variable1'), {target: {value: 'wrong'}})

        fireEvent.click(queryAllByText("+")[1])
        fireEvent.click(getByText("Submit and Provide a Solution"))
        await waitForExpect(() => expect(window.sessionStorage.setItem).toHaveBeenCalledTimes(1))
    })
})