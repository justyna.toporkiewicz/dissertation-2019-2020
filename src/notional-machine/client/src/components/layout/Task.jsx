import React from 'react'
import { Link } from 'react-router-dom'
import TaskButton from './TaskButton'

/** Represents a generic task **/
const Task = ({ children, next, id }) => {
    console.log( "the id is: ", id )
  return (
      <div class="row">
        <div class="col-sm-2">
          <thead>
            <h2>Jump to...</h2>
          </thead>
          <tbody>
            <hr/>
            <tr><Link to='/home/'>Home</Link></tr>
            <tr><Link to='/exercise-list/'>Exercise List</Link></tr>
            <tr><Link to='/admin/exercise-list/'>Create Exercises</Link></tr>
            <tr><hr/></tr>
            <tr><h4>This exercise</h4></tr>
            <tr><Link to={"/exercise/" + id + "/task/1"}>Expression task</Link></tr>
            <tr><Link to={"/exercise/" + id + "/task/2"}>Control Flow task</Link></tr>
            <tr><Link to={"/exercise/" + id + "/task/3"}>Execution task</Link></tr>
          </tbody>
        </div>
        <div className="Task">
          <div className="container-fluid">{children}</div>
          <TaskButton {...next} />
        </div>
      </div>
  )
}

Task.defaultProps = {
  next: { to: '/', text: 'Next' }
}

export default Task
