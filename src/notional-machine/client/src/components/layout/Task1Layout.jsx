import React from 'react'
import Task from './Task'
import TaskInstructions from './TaskInstructions'
import CodeFragment from '../fragment/CodeFragment'

/** Extends the generic task to represent the layout of task 1 **/
const Task1Layout = (props) => {

    if( !props.checking ){
        return (
            <div className="Task1">
                <Task next={props.next} id={props.data.exercise.id}>
                    <TaskInstructions>
                        <h4>Step 1: Identifying Expressions</h4>
                        <ul>
                            <li>Task: Read the following piece of code carefully and select all <em>expressions </em>
                                that are used in its construction.</li>
                            <li>To select an expression: Click on the two ends of the expression.</li>
                            <li>When you are done: Click 'Check Expressions'.</li>
                        </ul>

                    </TaskInstructions>
                    <div className="blackText">
                        <CodeFragment
                            fragment={props.data.exercise.code_fragment}
                            expressions={props.data.expressions}
                            {...props}
                        />
                    </div>
                </Task>
            </div>
        )
    } else {
        return (
            <div className="Task1">
                <Task next={props.next} id={props.data.exercise.id}>
                    <TaskInstructions>
                        <h4>Step 1: Identifying Expressions - did you get them correct?</h4>
                        <ul>
                            <li>Solid green: Your selection or part of your selection is correct.</li>
                            <li>Solid red: Your selection or part of it is NOT correct.</li>
                            <li>Red outline: You missed an expression or part of one.</li>
                        </ul>

                    </TaskInstructions>
                    <div className="blackText">
                        <CodeFragment
                            fragment={props.data.exercise.code_fragment}
                            expressions={props.data.expressions}
                            {...props}
                        />
                    </div>
                </Task>
            </div>
        )
    }
}

export default Task1Layout
