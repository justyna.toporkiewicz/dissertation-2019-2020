import React from 'react'
import Loading from '../DefaultPages/Loading'
import client from '../api-client'
import * as cookie from '../cookies'
import * as Constant from '../Constants'
import { USER_ID_COOKIE_NAME } from '../config'

function getPupilId () {
    return cookie.get(USER_ID_COOKIE_NAME)
}

async function fetchExpressionExercise (id) {
    return client.resource('expression_exercise').get(id)
}

async function fetchExpressionUnderlines (exerciseId, studentId) {
    return await client.resource('expression_underline').find({ exercise_id: exerciseId, student_id: studentId})
}

async function fetchExpressionUnderlineValues (exerciseId, studentId) {
    return await client.resource('expression_underline_values').find({ exercise_id: exerciseId, student_id: studentId})
}

async function fetchExpressionVariables (exerciseId) {
    return client.resource('expression_variable').find({ exercise_id: exerciseId})
}

function tokenize (text) {
    return text.split('')
}

function splitComment (text) {
    return text.split('\n')
}

function createSolutionsVariable (length) {
    var dictionary = {}
    for (var index = 0; index < length; index++){
      dictionary["line"+index] = ""
    }
    return dictionary
}

function cleanUnderline (list, start, end) {
    let indices = [start, end]
    if (list[0] == " ") {
      indices[0] = indices[0] + list.join('').search(/\S/)
      list = list.slice(list.join('').search(/\S/))
    }
    if (list[list.length-1] == " ") {
      let newList = list.join('').trim()
      indices[1] = indices[1] - (list.length - newList.length)
    }
    return indices
}

function cleanSolution (solution) {
    return solution.trim()
}
  
function prepareUnderlines (model) {
    let list = []
    for (let underline = 0; underline < model.length; underline++) {
      list = list.concat([[model[underline].startIndex, model[underline].endIndex]])
    }
    return list
  }

function prepareValues (values) {
    let cleanValues = {}
    for (let line = 0; line < values.length; line++) {
        cleanValues["line"+line] = values[line].value
    }
    return cleanValues
}

function evaluationCorrectness (correct, given) {
    var status = []
    for (var index = 0; index < Object.keys(correct).length; index++){
        status = status.concat([[
            correct["line"+index] == given["line"+index] ?
                Constant.CORRECT :
                Constant.WRONG,
            correct["line"+index]]])
    }
    return status
}

function isUnderlineCorrect (underlinesSet1, underlinesSet2, version) {
    var correct = []
    var alreadySeen = []
    for (var line = 0; line < underlinesSet1.length; line++) {
        correct = version == "correctUnderlines" ?
            correct.concat([[Constant.MISSING, Constant.MISSING]]) :
            correct.concat([[Constant.WRONG, Constant.WRONG]])
        const lineCheck = underlinesSet1[line]
        if(alreadySeen[lineCheck]) continue
        else alreadySeen[lineCheck] = true

        for (var check = 0; check < underlinesSet2.length; check++) {
            const checkAgainst = underlinesSet2[check]
            if (lineCheck[0] == checkAgainst[0] && lineCheck[1] == checkAgainst[1]) {
                line == check ?
                    correct[line] = [Constant.CORRECT, Constant.CORRECT] :
                    correct[line] = [Constant.CORRECT, version == "correctUnderlines" ?
                        Constant.MISSING :
                        Constant.WRONG]
                break
            }
        }
    }
    return correct
}

export default function prepareData (ChildComponent, options = {}) {
    return class HydratableComponent extends React.Component {
        constructor () {
            super()
            this.state = {
                dataCollected: false,
                data: {}
            }
        }

        async prepare () {
            const exerciseId = this.props.match.params.id
            const exerciseDetails = await fetchExpressionExercise(exerciseId)

            let exercise = {}
            exercise.expression = exerciseDetails.expression
            exercise.id = exerciseDetails.id
            if(options.status == Constant.PART_1) {
                exercise.comment = exerciseDetails.part1_comment
            }
            else if (options.status == Constant.PART_2) {
                exercise.comment = exerciseDetails.part2_comment
            }
            const studentId = "studentId" in sessionStorage ? sessionStorage.getItem("studentId") : getPupilId()

            let studentSolution = []
            let modelSolution = []
            switch(options.status) {
                case Constant.PART_1_SOLUTION:
                    modelSolution = [await fetchExpressionUnderlines(exerciseId, 'NULL'), []]
                    studentSolution = [await fetchExpressionUnderlines(exerciseId, studentId), []]
                    break
                case Constant.PART_2:
                    modelSolution = [await fetchExpressionUnderlines(exerciseId, 'NULL'), []]
                    break
                case Constant.PART_1:
                case Constant.PART_2_SOLUTION:
                    modelSolution = [await fetchExpressionUnderlines(exerciseId, 'NULL'),
                        await fetchExpressionUnderlineValues(exerciseId, 'NULL')]
                    studentSolution = [await fetchExpressionUnderlines(exerciseId, studentId),
                    await fetchExpressionUnderlineValues(exerciseId, studentId)]
            }

            const variables = await fetchExpressionVariables(exerciseId)
            
            this.setState({
                dataCollected: true,
                data: Object.assign({}, {exercise, variables, studentId, modelSolution, studentSolution})
            })
        }
        async componentDidMount () {
            await this.prepare()
        }

        async resetSolutions (solution, userId, callNo) {
            if (userId == 'NULL' && callNo == 2){return}
            
            let underlines = solution[0]
            for (let i = 0; i < underlines.length; i++) {
                await client.resource('expression_underline').remove(underlines[i].id)
            }
            if (solution.length == 2) {
                let values = solution[1]
                for (let i = 0; i < values.length; i++) {
                    await client.resource('expression_underline_values').remove(values[i].id)
                }
            }
        }

        render () {
            if (!this.state.dataCollected) {
                return (<Loading />)
            }
            return (<ChildComponent
                tokenize={tokenize}
                splitComment={splitComment}
                createSolutionsVariable={createSolutionsVariable}
                cleanUnderline={cleanUnderline}
                cleanSolution={cleanSolution}
                prepareUnderlines={prepareUnderlines}
                prepareValues={prepareValues}
                evaluationCorrectness={evaluationCorrectness}
                isUnderlineCorrect={isUnderlineCorrect}
                {...this.props}
                {...this.state}
                resetSolutions={this.resetSolutions}
                />)
        }
    }
}