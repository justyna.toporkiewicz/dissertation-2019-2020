import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import ExpressionExercisePart1 from './ExpressionExercisePart1'
import ExpressionExercisePart2 from './ExpressionExercisePart2'

import ExpressionExercisePart1Solution from './Solutions/ExpressionExercisePart1Solution'
import ExpressionExercisePart2Solution from './Solutions/ExpressionExercisePart2Solution'

import * as Constant from '../Constants'

const ExpressionExercisePage = ({ match }) => (
    <Switch>
        <Redirect
            from={'/expression-exercise/'+ match.params.id}
            exact to={{
                pathname: '/expression-exercise/'+ match.params.id + "/1",
                state: Constant.TEACHER_CREATE}}
        />

        <Route exact path="/expression-exercise/:id/1" component={ExpressionExercisePart1} />
        <Route exact path="/expression-exercise/:id/2" component={ExpressionExercisePart2} />

        <Route exact path="/expression-exercise/:id/1/solution" component={ExpressionExercisePart1Solution} />
        <Route exact path="/expression-exercise/:id/2/solution" component={ExpressionExercisePart2Solution} />
    </Switch>
)

export default ExpressionExercisePage
