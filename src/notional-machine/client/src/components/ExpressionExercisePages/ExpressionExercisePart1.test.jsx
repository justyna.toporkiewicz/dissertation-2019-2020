import React from 'react'
import { Router } from 'react-router-dom'
import { createMemoryHistory } from 'history'
import { render, fireEvent } from '@testing-library/react'
import ExpressionExercisePart1 from './ExpressionExercisePart1'
import * as Constant from '../Constants'
import client from '../api-client'
import waitForExpect from 'wait-for-expect'
import 'jest-canvas-mock'

jest.mock('../api-client')

describe("ExpressionExercisePart1", () => {

    Object.defineProperty(window.document, "cookie", {
        value: {
            split: () => (["nm_user=1"]),
            charAt: () => ("n"),
            indexOf: () => (0),
            substring: () => ("1")
        },
        writable: true
    })

    Object.defineProperty(window, 'sessionStorage', {
        value: {
          getItem: jest.fn(key => Constant.STUDENT),
          setItem: jest.fn((mode, teacher) => (mode, teacher)),
        },
        writable: true
      })
    const historyMock = { push: jest.fn()}
    let removed = false
    client.resource.mockImplementation((name)=>({
        get: () => ({
            name: "first",
            expression: "a + 9 - b",
            part1_comment: "simple expression",
            part2_comment: "simple variables",
        }), 
        find: (exercise) => {
            if (Object.keys(exercise).length == 2) {
                if(exercise.student_id != 'NULL') {
                    return[{startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                        {startIndex: 0, endIndex: 9, student_id: exercise.student_id},
                    ]
                }
                return[{startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                    {startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                    {startIndex: 0, endIndex: 9, student_id: exercise.student_id},
                    {startIndex: 3 , endIndex: 9, student_id: exercise.student_id},
                ]
            }
            return [
                { variable: "a = 4", id: "1" },
                { variable: "b = 5", id: "2" }
            ]
        },
        create: () => ({id: "1"}),
        remove: () => {removed = true}
    }))

    it("renders with all elements", async () => {
        const { getByText } = render(<ExpressionExercisePart1 match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Part 1: Select sub-expressions")).toBeTruthy())
        expect(getByText("simple expression")).toBeTruthy()
        expect(getByText("a")).toBeTruthy()
        expect(getByText("Submit")).toBeTruthy()
    })

    it("allows creating new underlines", async () => {
        const { getByText } = render(<ExpressionExercisePart1 match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Part 1: Select sub-expressions")).toBeTruthy())
        fireEvent.click(getByText("a"))
        fireEvent.click(getByText("9"))
        expect(getByText("1")).toBeTruthy()
        expect(getByText("Delete All Underlines")).toBeTruthy()
    })

    it("allows to move the underlines and delete them", async () => {
        const { getByText, queryByLabelText, queryByText } = render(<ExpressionExercisePart1 match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Part 1: Select sub-expressions")).toBeTruthy())
        fireEvent.click(getByText("a"))
        fireEvent.click(getByText("9"))

        fireEvent.click(getByText("b"))
        fireEvent.click(getByText("b"))

        fireEvent.click(getByText("b"))
        fireEvent.click(getByText("a"))

        fireEvent.click(queryByLabelText("underline2"))
        fireEvent.click(getByText("Move Up"))
        fireEvent.click(queryByLabelText("underline3"))
        fireEvent.click(getByText("Move Up"))
        fireEvent.click(queryByLabelText("underline2"))
        fireEvent.click(getByText("Move Down"))
        fireEvent.click(queryByLabelText("underline1"))
        fireEvent.click(getByText("Move Down"))
        fireEvent.click(queryByLabelText("underline3"))
        fireEvent.click(getByText("Delete"))

        expect(queryByLabelText("underline3")).toBeFalsy()

        fireEvent.click(queryByLabelText("underline1"))
        fireEvent.click(getByText("Delete"))

        expect(queryByLabelText("underline2")).toBeFalsy()
        
        fireEvent.click(getByText("Delete All Underlines"))
        expect(queryByLabelText("underline1")).toBeFalsy()
        expect(queryByText("Delete All Underlines")).toBeFalsy()
    })
    
    it("submits on click", async () => {
        const historySpy = jest.spyOn(historyMock, 'push')
        const { getByText } = render(<ExpressionExercisePart1 history={historyMock} match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Part 1: Select sub-expressions")).toBeTruthy())
        fireEvent.click(getByText("a"))
        fireEvent.click(getByText("9"))
        fireEvent.click(getByText("Submit"))
        await waitForExpect(() => expect(historySpy).toBeCalled())
    })
    
    it("renders without comments", async () => {
        client.resource.mockImplementation((name)=>({
            get: () => ({
                name: "first",
                expression: "a + 9 - b",
                part1_comment: "",
                part2_comment: "simple variables",
            }), 
            find: (exercise) => {
                if (Object.keys(exercise).length == 2) {
                    if(exercise.student_id != 'NULL') {
                        return[{startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                            {startIndex: 0, endIndex: 9, student_id: exercise.student_id},
                        ]
                    }
                    return[{startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                        {startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                        {startIndex: 0, endIndex: 9, student_id: exercise.student_id},
                        {startIndex: 3 , endIndex: 9, student_id: exercise.student_id},
                    ]
                }
                return [
                    { variable: "a = 4", id: "1" },
                    { variable: "b = 5", id: "2" }
                ]
            },
            create: () => ({id: "1"}),
            remove: () => ({})
        }))
        const { getByText, queryByText } = render(<ExpressionExercisePart1 match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Part 1: Select sub-expressions")).toBeTruthy())
        expect(queryByText("simple expression")).toBeFalsy()
        expect(getByText("Submit")).toBeTruthy()
    })

    it("submits correctly for teachers", async () => {        
        Object.defineProperty(window, 'sessionStorage', {
            value: {
            getItem: jest.fn(key => Constant.TEACHER_UPDATE),
            setItem: jest.fn((mode, teacher) => (mode, teacher)),
            },
            writable: true
        })
        const historySpy = jest.spyOn(historyMock, 'push')
        const { getByText } = render(<ExpressionExercisePart1 history={historyMock} match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Part 1: Select sub-expressions")).toBeTruthy())
        fireEvent.click(getByText("a"))
        fireEvent.click(getByText("9"))
        fireEvent.click(getByText("Submit"))
        await waitForExpect(() => expect(historySpy).toBeCalled())
    })

    it("doesn't show the exercise when not logged in", async () => {
        Object.defineProperty(window.document, "cookie", {
            value: {
                split: () => (["nm_user=null"]),
                charAt: () => ("n"),
                indexOf: () => (0),
                substring: () => ("1")
            },
            writable: true
        })
    
        const history = createMemoryHistory()
        const { getByText } = render(
            <Router history={history}>
                <ExpressionExercisePart1 match={{params: {id: "1"}}}/>
            </Router>
        )
        await waitForExpect(() => expect(getByText("You cannot view this page until you have logged in")).toBeTruthy())
    })



})