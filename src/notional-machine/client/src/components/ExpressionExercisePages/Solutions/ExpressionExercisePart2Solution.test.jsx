import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import ExpressionExercisePart2Solution from './ExpressionExercisePart2Solution'
import * as Constant from '../../Constants'
import client from '../../api-client'
import waitForExpect from 'wait-for-expect'
import 'jest-canvas-mock'

jest.mock('../../api-client')

describe("ExpressionExercisePart2Solution", () => {
    let foundOnce = false;

    Object.defineProperty(window.document, "cookie", {
        value: {
            split: () => (["nm_user=1"]),
            charAt: () => ("n"),
            indexOf: () => (0),
            substring: () => ("1")
        },
        writable: true
    })

    Object.defineProperty(window, 'sessionStorage', {
        value: {
          getItem: jest.fn(key => Constant.STUDENT),
          setItem: jest.fn((mode, teacher) => (mode, teacher)),
          removeItem: jest.fn()
        },
        writable: true
      })
    const historyMock = { push: jest.fn()}
    client.resource.mockImplementation((name)=>({
        get: () => ({
            name: "first",
            expression: "a + 9 - b",
            part1_comment: "simple expression",
            part2_comment: "simple variables",
        }), 
        find: (exercise) => {
            if (Object.keys(exercise).length == 2) {
                if(exercise.student_id != 'NULL') {
                    if(!foundOnce) {
                        foundOnce = true
                        return[{startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                            {startIndex: 0, endIndex: 9, student_id: exercise.student_id},
                        ]
                    }
                    foundOnce = false
                    return [{value: "19", student_id: exercise.student_id},
                    {value: "21", student_id: exercise.student_id},
                    ]
                }
                if(!foundOnce) {
                    foundOnce = true
                    return [{startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                        {startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                        {startIndex: 0, endIndex: 9, student_id: exercise.student_id},
                        {startIndex: 3 , endIndex: 9, student_id: exercise.student_id},
                    ]
                }
                foundOnce = false
                return[{value: "19", student_id: exercise.student_id},
                    {value: "15", student_id: exercise.student_id},
                    {value: "10", student_id: exercise.student_id},
                    {value: "20", student_id: exercise.student_id},
                ]
            }
            return [
                { variable: "a = 4" },
                { variable: "b = 5"}
            ]
        },
        create: () => ({id: "1"}),
        remove: () => ({})
    }))
    it("renders all correct elements", async () => {
        const { getByText } = render(<ExpressionExercisePart2Solution match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Solution Part 2")).toBeTruthy())
        expect(getByText("Variables")).toBeTruthy()
        expect(getByText("CORRECT SOLUTION")).toBeTruthy()
        expect(getByText("YOUR SOLUTION")).toBeTruthy()
        expect(getByText("Continue")).toBeTruthy()
    })

    it("allows students to submit a comment", async () => {
        const { getByText, getByLabelText } = render(<ExpressionExercisePart2Solution history={historyMock} match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Solution Part 2")).toBeTruthy())
        expect(fireEvent.change(getByLabelText('comment'), {target: {value: 'hello'}})).toBeTruthy()
        fireEvent.click(getByText("Continue"))
        await waitForExpect(() => expect(client.resource).toBeCalled())
    })

    it("continues when continue is clicked", async () => {
        const historySpy = jest.spyOn(historyMock, 'push')
        const { getByText } = render(<ExpressionExercisePart2Solution history={historyMock} match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Solution Part 2")).toBeTruthy())
        fireEvent.click(getByText("Continue"))
        await waitForExpect(() => expect(historySpy).toBeCalled())
    })

    it("renders without variables and comments", async () => {
        client.resource.mockImplementation((name)=>({
            get: () => ({
                name: "first",
                expression: "a + 9 - b",
                part1_comment: "",
                part2_comment: "",
            }), 
            find: (exercise) => {
                if (Object.keys(exercise).length == 2) {
                    if(exercise.student_id != 'NULL') {
                        if(!foundOnce) {
                            foundOnce = true
                            return[{startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                                {startIndex: 0, endIndex: 9, student_id: exercise.student_id},
                            ]
                        }
                        foundOnce = false
                        return [{value: "19", student_id: exercise.student_id},
                        {value: "21", student_id: exercise.student_id},
                        ]
                    }
                    if(!foundOnce) {
                        foundOnce = true
                        return [{startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                            {startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                            {startIndex: 0, endIndex: 9, student_id: exercise.student_id},
                            {startIndex: 3 , endIndex: 9, student_id: exercise.student_id},
                        ]
                    }
                    foundOnce = false
                    return[{value: "19", student_id: exercise.student_id},
                        {value: "15", student_id: exercise.student_id},
                        {value: "10", student_id: exercise.student_id},
                        {value: "20", student_id: exercise.student_id},
                    ]
                }
                return []
            },
            create: () => ({id: "1"}),
            remove: () => ({})
        }))
        const { getByText, queryByText } = render(<ExpressionExercisePart2Solution match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Solution Part 2")).toBeTruthy())
        expect(queryByText("Variables")).toBeFalsy()
        expect(getByText("CORRECT SOLUTION")).toBeTruthy()
        expect(getByText("YOUR SOLUTION")).toBeTruthy()
        expect(getByText("Continue")).toBeTruthy()
    })

    it("shows up correctly when in teacher mode and with no comments", async () => {
        Object.defineProperty(window, 'sessionStorage', {
            value: {
              getItem: jest.fn(key => Constant.TEACHER_VIEW),
              setItem: jest.fn((mode, teacher) => (mode, teacher)),
              removeItem: jest.fn()
            },
            writable: true
          })
        const { getByText, queryByText } = render(<ExpressionExercisePart2Solution match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Expression Evaluation Exercise")).toBeTruthy())
        expect(queryByText("Variables")).toBeFalsy()
        expect(queryByText("YOUR SOLUTION")).toBeFalsy()
    })

    it("submits appropriately when in teacher mode", async () => {
        client.resource.mockImplementation((name)=>({
            get: () => ({
                name: "first",
                expression: "a + 9 - b",
                part1_comment: "",
                part2_comment: "",
            }), 
            find: (exercise) => {
                if (Object.keys(exercise).length == 2) {
                    if(exercise.student_id != 'NULL') {
                        if(!foundOnce) {
                            foundOnce = true
                            return[{startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                                {startIndex: 0, endIndex: 9, student_id: exercise.student_id},
                            ]
                        }
                        foundOnce = false
                        return [{value: "19", student_id: exercise.student_id},
                        {value: "21", student_id: exercise.student_id},
                        ]
                    }
                    if(!foundOnce) {
                        foundOnce = true
                        return [{startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                            {startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                            {startIndex: 0, endIndex: 9, student_id: exercise.student_id},
                            {startIndex: 3 , endIndex: 9, student_id: exercise.student_id},
                        ]
                    }
                    foundOnce = false
                    return[{value: "19", student_id: exercise.student_id},
                        {value: "15", student_id: exercise.student_id},
                        {value: "10", student_id: exercise.student_id},
                        {value: "20", student_id: exercise.student_id},
                    ]
                }
                return [
                    { variable: "a = 4" },
                    { variable: "b = 5"}
                ]
            },
            create: () => ({id: "1"}),
            remove: () => ({})
        }))

        const historySpy = jest.spyOn(historyMock, 'push')
        const { getByText, queryByText } = render(<ExpressionExercisePart2Solution history={historyMock} match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Expression Evaluation Exercise")).toBeTruthy())
        expect(queryByText("YOUR SOLUTION")).toBeFalsy()
        fireEvent.click(getByText("Continue"))
        await waitForExpect(() => expect(historySpy).toBeCalled())
    })

    it("shows student solution to the teacher in the correct mode", async () => {
        Object.defineProperty(window, 'sessionStorage', {
            value: {
              getItem: jest.fn(key => Constant.TEACHER_STUDENT_SOLUTION),
              setItem: jest.fn((mode, teacher) => (mode, teacher)),
              removeItem: jest.fn()
            },
            writable: true
          })
        client.resource.mockImplementation((name)=>({
            get: () => ({
                name: "first",
                expression: "a + 9 - b",
                part1_comment: "",
                part2_comment: "",
            }), 
            find: (exercise) => {
                if (Object.keys(exercise).length == 2) {
                    if(exercise.student_id != 'NULL') {
                        if(!foundOnce) {
                            foundOnce = true
                            return[{startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                                {startIndex: 0, endIndex: 9, student_id: exercise.student_id},
                            ]
                        }
                        foundOnce = false
                        return [{value: "19", student_id: exercise.student_id},
                        {value: "21", student_id: exercise.student_id},
                        ]
                    }
                    if(!foundOnce) {
                        foundOnce = true
                        return [{startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                            {startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                            {startIndex: 0, endIndex: 9, student_id: exercise.student_id},
                            {startIndex: 3 , endIndex: 9, student_id: exercise.student_id},
                        ]
                    }
                    foundOnce = false
                    return[{value: "19", student_id: exercise.student_id},
                        {value: "15", student_id: exercise.student_id},
                        {value: "10", student_id: exercise.student_id},
                        {value: "20", student_id: exercise.student_id},
                    ]
                }
                return [
                    { variable: "a = 4" },
                    { variable: "b = 5"}
                ]
            },
            create: () => ({id: "1"}),
            remove: () => ({})
        }))

        const historySpy = jest.spyOn(historyMock, 'push')
        const { getByText, queryByText } = render(<ExpressionExercisePart2Solution history={historyMock} match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Expression Evaluation Exercise")).toBeTruthy())
        expect(queryByText("STUDENT'S SOLUTION PART 1")).toBeTruthy()
        expect(queryByText("STUDENT'S SOLUTION PART 2")).toBeTruthy()
        fireEvent.click(getByText("Continue"))
        await waitForExpect(() => expect(historySpy).toBeCalled())
    })
})