import React from 'react'
import Underline from '../../ExpressionExerciseComponents/Underline'
import ExpressionToken from '../../ExpressionExerciseComponents/ExpressionToken'
import Submit from '../../ExpressionExerciseComponents/Submit'
import prepareData from '../prepareData'
import client from '../../api-client'
import "../ExpressionExercises.css"
import * as Constant from '../../Constants'
import { USER_ID_COOKIE_NAME } from '../../config'
import * as cookie from'../../cookies'
import NotLoggedIn from '../../DefaultPages/NotLoggedIn'

class ExpressionExercisePart2Solution extends React.Component {
    constructor(){
        super()
        this.state = {
            expression: [],
            underlines: [],
            studentUnderlines: [],
            variables: [],
            solutionValues: {},
            givenValues: {},
            status: '',
            comment: ""
        }
        this.correctness = []
        this.handleChange = this.handleChange.bind(this)
        this.continue = this.continue.bind(this)
    }

    UNSAFE_componentWillMount () {
        this.setState({
            expression: this.props.tokenize(this.props.data.exercise.expression),
            underlines: this.props.prepareUnderlines(this.props.data.modelSolution[0]),
            variables: this.props.data.variables,
            solutionValues: this.props.prepareValues(this.props.data.modelSolution[1]),
            givenValues: this.props.prepareValues(this.props.data.studentSolution[1]),
            status: sessionStorage.getItem(Constant.MODE)
        })

        if(sessionStorage.getItem(Constant.MODE) == Constant.TEACHER_STUDENT_SOLUTION) {
            this.setState({
                studentUnderlines: this.props.prepareUnderlines(this.props.data.studentSolution[0]),
            })
        }
    }

    handleChange(e) {
        this.setState({
            comment: e.target.value
        })
    }

    async continue (e) {
        e.preventDefault()
        if(this.state.status == Constant.STUDENT) {
            if(this.state.comment != "") {
                await client.resource('solution_comment').create({
                    exercise_id: this.props.data.exercise.id,
                    student_id: this.props.data.studentId,
                    comment_body: this.state.comment,
                    exercise_part: 2
                })
            }
            sessionStorage.removeItem(Constant.MODE)
            this.props.history.push('/exercise-list')
        }
        else {
            sessionStorage.removeItem(Constant.MODE)
            this.props.history.push('/admin/exercise-list')
        }
    }
    
    render() {
        if (cookie.get(USER_ID_COOKIE_NAME) == 'null') {
            return (<NotLoggedIn />)
        }
        this.correctness = this.state.status != Constant.TEACHER_VIEW ?
            this.props.evaluationCorrectness(this.state.solutionValues,
                this.state.givenValues) :
            this.props.evaluationCorrectness(this.state.solutionValues,
                this.state.solutionValues)
        
        this.underlineCorrectness = this.state.status == Constant.TEACHER_STUDENT_SOLUTION ? [
            this.props.isUnderlineCorrect(this.state.underlines,
                this.state.studentUnderlines,
                "correctUnderlines"),
            this.props.isUnderlineCorrect(this.state.studentUnderlines,
                this.state.underlines,
                "submittedUnderlines")] :
            []

        return (
            <form onSubmit={this.continue}>
                <div className="header centerText">
                {this.state.status == Constant.STUDENT ? 
                    <div>
                        <h2><b>Solution Part 2</b></h2>
                        <p>Meaning of the colouring of values in the solutions below:</p>
                        <p>Green: this is the correct value for this underline</p>
                        <p>Red: appears only in Your Solution, indicating an incorrect value</p>
                        <p>Yellow: appears only in Correct Solution, indicating the correct value for a value incorrectly given in Your Solution</p>
                    </div>
                    : this.state.status == Constant.TEACHER_STUDENT_SOLUTION ?
                        <div>
                            <h2><b>Expression Evaluation Exercise</b></h2>
                            <p>This is the solution provided by the student compared to the correct solution</p>
                        </div>
                        : <div>
                            <h2><b>Expression Evaluation Exercise</b></h2>
                            <p>This is the teacher submitted solution to the expression evaluation exercise.</p>
                            <p>If you believe this solution is incorrect you can update it by going to the admin exercise list</p>
                            <p>and choosing the "Update exercise solution" option form the exercise menu</p>
                        </div>
                }
                
                </div>
                <div className="exerciseRow">
                    {this.state.variables.length != 0 ? 
                        <div className="box col-sm-2">
                            <p><b>Variables</b></p>
                            {this.state.variables.map((variab, i) =>
                                <div>
                                    <pre key={i}>{variab.variable}</pre>
                                    <p/>
                                </div>
                            )}
                        </div> : null
                    }
                </div>
                <div className="exerciseRow">
                    <div className={"box expressionBoxWidth " + (this.state.status == Constant.STUDENT ? 
                            "side" : "body")}>
                        <p className="centerText"><b>CORRECT SOLUTION</b></p>
                        {this.state.expression.map((token, i) => 
                        <ExpressionToken
                            key={i}
                            content={token}
                            index={i}
                        />
                        )}
                        {this.state.underlines.map((line, i) =>
                            <Underline
                            key={i}
                            startIndex={line[0]}
                            endIndex={line[1]}
                            index={i}
                            status={this.state.status != Constant.TEACHER_STUDENT_SOLUTION ? Constant.PART_2_SOLUTION : this.underlineCorrectness[0][i][0]}
                            labelStatus={this.state.status != Constant.TEACHER_STUDENT_SOLUTION ? Constant.PART_2_SOLUTION : this.underlineCorrectness[0][i][1]}
                            evaluated={this.state.solutionValues["line"+i]}
                            evaluationCorrect={this.correctness[i][0] == Constant.CORRECT ?
                                Constant.CORRECT :
                                Constant.MISSING}
                            />
                        )}
                    </div>
                    {this.state.status != Constant.TEACHER_VIEW ? 
                        <div className="expressionBoxWidth bodyInLine">
                        {this.state.status == Constant.TEACHER_STUDENT_SOLUTION ?
                            <div className="innerBox">
                                <p className="centerText">
                                    <b>{this.state.status == Constant.STUDENT ? "YOUR SOLUTION" : "STUDENT'S SOLUTION PART 1"}</b>
                                </p>
                                {this.state.expression.map((token, i) => 
                                <ExpressionToken
                                    key={i}
                                    content={token}
                                    index={i}
                                />
                                )}
                                {this.state.studentUnderlines.map((line, i) =>
                                    <Underline
                                    key={i}
                                    startIndex={line[0]}
                                    endIndex={line[1]}
                                    index={i}
                                    status={this.underlineCorrectness[1][i][0]}
                                    labelStatus={this.underlineCorrectness[1][i][1]}
                                    />
                                )}
                            </div>
                        : null}
                            <div className="innerBox">
                                <p className="centerText">
                                    <b>{this.state.status == Constant.STUDENT ? "YOUR SOLUTION" : "STUDENT'S SOLUTION PART 2"}</b>
                                </p>
                                {this.state.expression.map((token, i) => 
                                <ExpressionToken
                                    key={i}
                                    content={token}
                                    index={i}
                                />
                                )}
                                {this.state.underlines.map((line, i) =>
                                    <Underline
                                    key={i}
                                    startIndex={line[0]}
                                    endIndex={line[1]}
                                    index={i}
                                    status={Constant.PART_2_SOLUTION}
                                    labelStatus={Constant.PART_2_SOLUTION}
                                    evaluated={this.state.givenValues["line"+i]}
                                    evaluationCorrect={this.correctness[i][0]}
                                    />
                                )}
                            </div>
                        </div>
                    : null
                    }
                    
                </div>
                {this.state.status == Constant.STUDENT ? 
                    <div>
                        <hr className="separate"></hr>
                        <div className="exerciseRow">
                            <div className="box body">
                            <p><b>Comments from student</b> (optional)</p>
                            <p>eg. on gained understanding or missunderstandings</p>
                            <textarea
                                className="studentComment"
                                onChange={this.handleChange}
                                rows={5}
                                aria-label="comment"/>
                            </div>
                        </div>
                    </div>
                : null }
                <Submit value={"Continue"} />
            </form>
        )
    }
}

export { ExpressionExercisePart2Solution }

export default prepareData(ExpressionExercisePart2Solution, { status: Constant.PART_2_SOLUTION })