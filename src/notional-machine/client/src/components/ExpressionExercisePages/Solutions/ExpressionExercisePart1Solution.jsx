import React from 'react'
import Underline from '../../ExpressionExerciseComponents/Underline'
import ExpressionToken from '../../ExpressionExerciseComponents/ExpressionToken'
import Submit from '../../ExpressionExerciseComponents/Submit'
import NotLoggedIn from '../../DefaultPages/NotLoggedIn'
import prepareData from '../prepareData'
import client from '../../api-client'
import "../ExpressionExercises.css"
import { USER_ID_COOKIE_NAME } from '../../config'
import * as Constant from '../../Constants'
import * as cookie from '../../cookies'

class ExpressionExercisePart1Solution extends React.Component {
    constructor(){
        super()
        this.state = {
            expression: [],
            correctUnderlines: [],
            underlines: [],
            comment: "",
        }
        this.status = []
        this.continue = this.continue.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    UNSAFE_componentWillMount () {
        this.setState({
            expression: this.props.tokenize(this.props.data.exercise.expression),
            underlines: this.props.prepareUnderlines(this.props.data.studentSolution[0]),
            correctUnderlines: this.props.prepareUnderlines(this.props.data.modelSolution[0])
        })
    }

    handleChange(e) {
        this.setState({
            comment: e.target.value
        })
    }

    async continue (e) {
        e.preventDefault()
        if(this.state.comment != "") {
            await client.resource('solution_comment').create({
                exercise_id: this.props.data.exercise.id,
                student_id: this.props.data.studentId,
                comment_body: this.state.comment,
                exercise_part: 1
            })
        }
        this.props.history.push('/expression-exercise/'+this.props.data.exercise.id+'/2')
    }

    render() {
        if (cookie.get(USER_ID_COOKIE_NAME) == 'null') {
            return (<NotLoggedIn />)
        }
        this.status = [
            this.props.isUnderlineCorrect(this.state.correctUnderlines,
                this.state.underlines, "correctUnderlines"),
            this.props.isUnderlineCorrect(this.state.underlines,
                this.state.correctUnderlines, "submittedUnderlines")]
        return (
            <form onSubmit={this.continue}>
                <div className="header centerText">
                    <h2>Solution to Part 1</h2>
                    <p>On the left is the expected solution and on the right is the solution provided by you.</p>
                    <p>Mouse over the coloured underlines and labels for pop-up explanations of what they mean</p>
                </div>
                <div className="exerciseRow">
                    <div className="box expressionBoxWidth side">
                        <p className="centerText"><b>CORRECT SOLUTION</b></p>
                        {this.state.expression.map((token, i) =>
                            <ExpressionToken
                                key={i}
                                content={token}
                                index={i}
                            />
                        )}
                        {this.state.correctUnderlines.map((line, i) => (
                            <Underline
                                key={i}
                                startIndex={line[0]}
                                endIndex={line[1]}
                                index={i}
                                status={this.status[0][i][0]}
                                labelStatus={this.status[0][i][1]}
                                part1solution={Constant.TEACHER}
                            />
                        ))}
                    </div>
                    <div className="box expressionBoxWidth bodyInLine">
                        <p className="centerText"><b>YOUR SOLUTION</b></p>
                        {this.state.expression.map((token, i) =>
                            <ExpressionToken 
                                key={i}
                                content={token}
                                index={i}
                            />
                        )}
                        {this.state.underlines.map((line, i) => (
                            <Underline
                                key={i}
                                startIndex={line[0]}
                                endIndex={line[1]}
                                index={i}
                                status={this.status[1][i][0]}
                                labelStatus={this.status[1][i][1]}
                                part1solution={Constant.STUDENT}
                            />
                        ))}
                    </div>
                </div>
                <hr className="separate"></hr>
                <div className="exerciseRow">
                    <div className="box body">
                        <p><b>Comments from student</b> (optional)</p>
                        <p>eg. on gained understanding or missunderstandings</p>
                        <textarea
                            className="studentComment"
                            onChange={this.handleChange}
                            rows={5}
                            aria-label="comment" />
                    </div>
                </div>
                <Submit value={"Continue"} />
            </form>
        )
    }
}

export { ExpressionExercisePart1Solution }

export default prepareData(ExpressionExercisePart1Solution, { status: Constant.PART_1_SOLUTION })