import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import ExpressionExercisePart1Solution from './ExpressionExercisePart1Solution'
import * as Constant from '../../Constants'
import client from '../../api-client'
import waitForExpect from 'wait-for-expect'
import 'jest-canvas-mock'

jest.mock('../../api-client')

describe("ExpressionExercisePart1Solution", () => {
    Object.defineProperty(window.document, "cookie", {
        value: {
            split: () => (["nm_user=1"]),
            charAt: () => ("n"),
            indexOf: () => (0),
            substring: () => ("1")
        },
        writable: true
    })

    Object.defineProperty(window, 'sessionStorage', {
        value: {
          getItem: jest.fn(key => Constant.STUDENT),
          setItem: jest.fn((mode, teacher) => (mode, teacher)),
        },
        writable: true
      })
    const historyMock = { push: jest.fn()}
    client.resource.mockImplementation((name)=>({
        get: () => ({
            name: "first",
            expression: "a + 9 - b",
            part1_comment: "simple expression",
            part2_comment: "simple variables",
        }), 
        find: (exercise) => {
            if (Object.keys(exercise).length == 2) {
                if(exercise.student_id != 'NULL') {
                return[{startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                    {startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                    {startIndex: 0, endIndex: 9, student_id: exercise.student_id},
                    {startIndex: 3 , endIndex: 9, student_id: exercise.student_id}]
                }
                return[{startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                    {startIndex: 0, endIndex: 9, student_id: exercise.student_id},
                    {startIndex: 0, endIndex: 0, student_id: exercise.student_id},
                    {startIndex: 7 , endIndex: 9, student_id: exercise.student_id},
                ]
            }
            return [
                { name: "a", value: "4" },
                { name: "b", value: "5"}
            ]
        },
        create: () => ({id: "1"}),
        remove: () => ({})
    }))
    it("renders all correct elements", async () => {
        const { getByText } = render(<ExpressionExercisePart1Solution match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Solution to Part 1")).toBeTruthy())
        expect(getByText("CORRECT SOLUTION")).toBeTruthy()
        expect(getByText("YOUR SOLUTION")).toBeTruthy()
        expect(getByText("Continue")).toBeTruthy()
    })

    it("allows students to submit a comment", async () => {
        const { getByText, getByLabelText } = render(<ExpressionExercisePart1Solution history={historyMock} match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Solution to Part 1")).toBeTruthy())
        expect(fireEvent.change(getByLabelText('comment'), {target: {value: 'hello'}})).toBeTruthy()
        fireEvent.click(getByText("Continue"))
        await waitForExpect(() => expect(client.resource).toBeCalled())
    })

    it("continues when continue is clicked", async () => {
        const historySpy = jest.spyOn(historyMock, 'push')
        const { getByText } = render(<ExpressionExercisePart1Solution history={historyMock} match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Solution to Part 1")).toBeTruthy())
        fireEvent.click(getByText("Continue"))
        await waitForExpect(() => expect(historySpy).toBeCalled())
    })
})