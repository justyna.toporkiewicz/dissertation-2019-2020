import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import ExpressionExercisePart2 from './ExpressionExercisePart2'
import * as Constant from '../Constants'
import client from '../api-client'
import waitForExpect from 'wait-for-expect'
import 'jest-canvas-mock'

jest.mock('../api-client')

describe("ExpressionExercisePart2", () => {

    let foundOnce = false;

    Object.defineProperty(window.document, "cookie", {
        value: {
            split: () => (["nm_user=1"]),
            charAt: () => ("n"),
            indexOf: () => (0),
            substring: () => ("1")
        },
        writable: true
    })

    Object.defineProperty(window, 'sessionStorage', {
        value: {
          getItem: jest.fn(key => Constant.STUDENT),
          setItem: jest.fn((mode, teacher) => (mode, teacher)),
        },
        writable: true
      })
    const historyMock = { push: jest.fn()}
    client.resource.mockImplementation((name)=>({
        get: () => ({
            name: "first",
            expression: "a + 9 - b",
            part1_comment: "simple expression",
            part2_comment: "simple variables",
        }), 
        find: (exercise) => {
            if (Object.keys(exercise).length == 2) {
                if(exercise.student_id != 'NULL') {
                    if(!foundOnce) {
                        foundOnce = true
                        return[{startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                            {startIndex: 0, endIndex: 9, student_id: exercise.student_id},
                        ]
                    }
                    foundOnce = false
                    return [{value: "19", student_id: exercise.student_id},
                    {value: "21", student_id: exercise.student_id},
                    ]
                }
                if(!foundOnce) {
                    foundOnce = true
                    return [{startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                        {startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                        {startIndex: 0, endIndex: 9, student_id: exercise.student_id},
                        {startIndex: 3 , endIndex: 9, student_id: exercise.student_id},
                    ]
                }
                foundOnce = false
                return[{value: "19", student_id: exercise.student_id},
                    {value: "15", student_id: exercise.student_id},
                    {value: "10", student_id: exercise.student_id},
                    {value: "20", student_id: exercise.student_id},
                ]
            }
            return [
                { variable: "a = 4" },
                { variable: "b = 5"}
            ]
        },
        create: () => ({id: "1"}),
        remove: () => ({})
    }))

    it("renders with all elements", async () => {
        const { getByText } = render(<ExpressionExercisePart2 match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Part 2: Evaluate sub-expressions")).toBeTruthy())
        expect(getByText("simple variables")).toBeTruthy()
        expect(getByText("a")).toBeTruthy()
        expect(getByText("1")).toBeTruthy()
        expect(getByText("a = 4")).toBeTruthy()
        expect(getByText("Submit")).toBeTruthy()
    })

    it("allows values of the underlines to be changed", async () => {
        const { getByText, getByLabelText } = render(<ExpressionExercisePart2 match={{params: {id: "1"}}}/>)
        await waitForExpect(() => expect(getByText("Part 2: Evaluate sub-expressions")).toBeTruthy())
        expect(fireEvent.change(getByLabelText('underlineInput1'), {target: {value: '13'}})).toBeTruthy()
        expect(fireEvent.change(getByLabelText('underlineInput2'), {target: {value: '8'}})).toBeTruthy()
    })

    it("submits when clicked", async () => {
        const historySpy = jest.spyOn(historyMock, 'push')
        const { getByText, getByLabelText } = render(<ExpressionExercisePart2 history={historyMock} match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Part 2: Evaluate sub-expressions")).toBeTruthy())
        fireEvent.change(getByLabelText('underlineInput1'), {target: {value: '13'}})
        fireEvent.click(getByText("Submit"))
        await waitForExpect(() => expect(historySpy).toBeCalled())
    })

    it("renders correctly without comments and variables", async () => {
        client.resource.mockImplementation((name)=>({
            get: () => ({
                name: "first",
                expression: "a + 9 - b",
                part1_comment: "simple expression",
                part2_comment: "",
            }), 
            find: (exercise) => {
                if (Object.keys(exercise).length == 2) {
                    if(exercise.student_id != 'NULL') {
                        if(!foundOnce) {
                            console.log("underlines")
                            foundOnce = true
                            return[{startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                                {startIndex: 0, endIndex: 9, student_id: exercise.student_id},
                            ]
                        }
                        console.log("values")
                        foundOnce = false
                        return [{value: "19", student_id: exercise.student_id},
                        {value: "21", student_id: exercise.student_id},
                        ]
                    }
                    if(!foundOnce) {
                        foundOnce = true
                        return [{startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                            {startIndex: 0, endIndex: 5, student_id: exercise.student_id},
                            {startIndex: 0, endIndex: 9, student_id: exercise.student_id},
                            {startIndex: 3 , endIndex: 9, student_id: exercise.student_id},
                        ]
                    }
                    foundOnce = false
                    return[{value: "19", student_id: exercise.student_id},
                        {value: "15", student_id: exercise.student_id},
                        {value: "10", student_id: exercise.student_id},
                        {value: "20", student_id: exercise.student_id},
                    ]
                }
                return []
            },
            create: () => ({id: "1"}),
            remove: () => ({})
        }))
        const { getByText, queryByText } = render(<ExpressionExercisePart2 match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Part 2: Evaluate sub-expressions")).toBeTruthy())
        expect(queryByText("simple variables")).toBeFalsy()
        expect(getByText("a")).toBeTruthy()
        expect(getByText("1")).toBeTruthy()
        expect(queryByText("a = 4")).toBeFalsy()
        expect(getByText("Submit")).toBeTruthy()
    })

    it("renders correctly for a teacher", async () => {
        Object.defineProperty(window.document, "cookie", {
            value: {
                split: () => (["nm_user=NULL"]),
                charAt: () => ("n"),
                indexOf: () => (0),
                substring: () => ("NULL")
            },
            writable: true
        })

        Object.defineProperty(window, 'sessionStorage', {
            value: {
                getItem: jest.fn(key => Constant.TEACHER_UPDATE),
                setItem: jest.fn((mode, teacher) => (mode, teacher)),
                removeItem: jest.fn()
            },
            writable: true
        })

        const historySpy = jest.spyOn(historyMock, 'push')
        const { getByText, getByLabelText } = render(<ExpressionExercisePart2 history={historyMock} match={{params: {id: "1"}}} />)
        await waitForExpect(() => expect(getByText("Part 2: Evaluate sub-expressions")).toBeTruthy())
        fireEvent.change(getByLabelText('underlineInput1'), {target: {value: '13'}})
        fireEvent.click(getByText("Submit"))
        await waitForExpect(() => expect(historySpy).toBeCalled())
    })

})