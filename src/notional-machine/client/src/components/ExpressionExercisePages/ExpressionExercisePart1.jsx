import React from 'react'
import Underline from '../ExpressionExerciseComponents/Underline'
import { USER_ID_COOKIE_NAME } from '../config'
import ExpressionToken from '../ExpressionExerciseComponents/ExpressionToken'
import Submit from '../ExpressionExerciseComponents/Submit'
import "./ExpressionExercises.css"
import prepareData from './prepareData'
import client from '../api-client'
import * as Constant from '../Constants'
import * as cookie from'../cookies'
import NotLoggedIn from '../DefaultPages/NotLoggedIn'

class ExpressionExercisePart1 extends React.Component {
  
  constructor(){
    super()
    this.state = {
      expression: [],
      underlines: [],
      comments: "",
      underliningRange: false,
      startOfUnderline: -1,
      status: '',
    }
    this.underlineClick = this.underlineClick.bind(this)
    this.deleteUnderline = this.deleteUnderline.bind(this)
    this.moveLine = this.moveLine.bind(this)
    this.onSubmit = this.handleSubmit.bind(this)
  }

  UNSAFE_componentWillMount () {
    this.setState({
      expression: this.props.tokenize(this.props.data.exercise.expression),
      comments: this.props.data.exercise.comment,
      status: sessionStorage.getItem(Constant.MODE)
    })
    window.onpopstate = (e) => {
      try{
        if (Constant.MODE in sessionStorage) {
          if (sessionStorage.getItem(Constant.MODE) == Constant.TEACHER_CREATE) {
            this.deleteExercise(this.props.data.exercise.id)
          }
        }
      }
      catch {
        console.log("DID NOT DELETE ABANDONDED EXERCISE")
      }
      window.onpopstate = () => ({})
    }
  }

  async deleteExercise(id) {
    await client.resource('expression_exercise').remove(id)
    const variables = await client.resource('expression_variable').find({ exercise_id: id })
    for (const variab of variables) {
      if (variab.id) {
        await client.resource('expression_variable').remove(id = variab.id)
      }
    }
  }

  addToUnderlined (endRange) {
    this.setState({
      underliningRange: false,
      underlines: [...this.state.underlines, 
        this.getUnderline(this.state.startOfUnderline, endRange+1)],
      startOfUnderline: -1
    })
  }

  underlineClick (tokenIndex) {
    if (this.state.underliningRange == true) {
      this.addToUnderlined(tokenIndex)
    }
    else {
      this.setState({
        underliningRange: true,
        startOfUnderline: tokenIndex
      })
    }
  }

  getUnderline (startRange, endRange) {
    if (startRange >= endRange) {
      let tempRange = startRange+1
      startRange = endRange-1
      endRange = tempRange
    }
    let lineInfo = this.props.cleanUnderline(this.state.expression.slice(startRange, endRange), startRange, endRange)
    return lineInfo
  }

  deleteUnderline (index) {
    if(index==0) {
      this.setState({underlines: this.state.underlines.slice(1, this.state.underlines.length)})
    }
    else{
      this.setState({
        underlines: [...this.state.underlines.slice(0, index),
          ...this.state.underlines.slice(index+1, this.state.underlines.length)]
      })
    }
  }

  moveLine (index, moveUp) {
    if(moveUp) {
      if(index==this.state.underlines.length-1) {
        this.setState({
          underlines: [
            ...this.state.underlines.slice(0, index-1),
            this.state.underlines[index],
            this.state.underlines[index-1]]})
      }
      else {
        this.setState({
          underlines: [
            ...this.state.underlines.slice(0, index-1),
            this.state.underlines[index],
            this.state.underlines[index-1],
            ...this.state.underlines.slice(index+1)
          ]
        })
      }
    }
    else {
      if(index==0) {
        this.setState({
          underlines: [
            this.state.underlines[index+1],
            this.state.underlines[index],
            ...this.state.underlines.slice(index+2)]
        })
      }
      else {
        this.setState({
          underlines: [
            ...this.state.underlines.slice(0, index),
            this.state.underlines[index+1],
            this.state.underlines[index],
            ...this.state.underlines.slice(index+2)
          ]
        })
      }
    }
  }

  deleteAllUnderlines () {
    this.setState({underlines: []})
  }

  async handleSubmit (e) {
    e.preventDefault()
  
    window.onpopstate = () => ({})
  
    const userId = this.state.status == Constant.STUDENT ? this.props.data.studentId : 'NULL'

    if(sessionStorage.getItem(Constant.MODE) == Constant.TEACHER_UPDATE &&
      this.props.data.modelSolution[0].length != 0) await this.props.resetSolutions(this.props.data.modelSolution, 'NULL', 1)
    if(this.props.data.studentSolution[0].length != 0 ||
      this.props.data.studentSolution[1].length != 0) await this.props.resetSolutions(this.props.data.studentSolution, userId, 2)

    for (let line = 0; line < this.state.underlines.length; line++) {
      await client.resource('expression_underline').create({
        exercise_id: this.props.data.exercise.id, 
        startIndex: this.state.underlines[line][0], 
        endIndex: this.state.underlines[line][1],
        orderNo: line+1,
        student_id: userId,
      })
    }
    if (this.state.status == Constant.STUDENT) {
      this.props.history.push('/expression-exercise/'+this.props.data.exercise.id+'/1/solution')
    }
    else {this.props.history.push('/expression-exercise/'+this.props.data.exercise.id+'/2')}
  }

  render() {
    if (cookie.get(USER_ID_COOKIE_NAME) == 'null') {
      return (<NotLoggedIn />)
    }
    return (
      <form onSubmit={this.onSubmit}>
        <div className="header centerText">
          <h2><b>Part 1: Select sub-expressions</b></h2>
          <p>In the given expression underline every section to be evaluated (by clicking first and last character in the subexpression).</p>
          <p>Some of these sections may hold inner ones.</p>
          <p>If you want to delete or move an underline, click on it to show its menu.</p>
          <p>You can also delete all lines at once by clicking the 'Delete All Underlines' button</p>
          <p><b>Do not perfrom any calculations in this step.</b></p>
          <p><b>Make sure that the order of underlines is the same as the order of evaluation.</b></p>
        </div>
        <div className="exerciseRow">
        {this.state.comments ? 
            <div className="box col-sm-6">
              <p><b>Comments from the teacher</b></p>
              {this.props.splitComment(this.state.comments).map((line, i) =>
                <p key={i}>{line}</p>
              )}
            </div>
          : null}
        </div>
        <div className="exerciseRow">
          <div className="box expressionBoxWidth">
            <p><b>Expression</b></p>
          {this.state.expression.map((token, i) =>
            <ExpressionToken
              key={i}
              content={token}
              index={i}
              underlineFunction={this.underlineClick}
              hoverChange={true}
              changeColor={this.state.underliningRange && this.state.startOfUnderline == i}
            />
          )}
          {this.state.underlines.map((line, i) =>
              <Underline
                key={i}
                startIndex={line[0]}
                endIndex={line[1]}
                index={i}
                deleteFunction={this.deleteUnderline}
                moveFunction={this.moveLine}
                listLength={this.state.underlines.length}
                status={Constant.PART_1}
                />)}
          {this.state.underlines.length != 0 ? 
            <div className="btn btn-sm btn-outline-danger displayBlock body" onClick={()=>this.deleteAllUnderlines()}>
              Delete All Underlines
            </div> 
            : null}
        </div>
        }
        </div>
        <Submit />
      </form>
    )
  } 
}

export { ExpressionExercisePart1 }

export default prepareData(ExpressionExercisePart1, { status: Constant.PART_1 })