import React from 'react'
import Underline from '../ExpressionExerciseComponents/Underline'
import ExpressionToken from '../ExpressionExerciseComponents/ExpressionToken'
import Submit from '../ExpressionExerciseComponents/Submit'
import NotLoggedIn from '../DefaultPages/NotLoggedIn'
import "./ExpressionExercises.css"
import client from '../api-client'
import prepareData from './prepareData'
import { USER_ID_COOKIE_NAME } from '../config'
import * as Constant from '../Constants'
import * as cookie from '../cookies'

class ExpressionExercisePart2 extends React.Component {
  constructor(){
    super()
    this.state = {
      expression: [],
      underlines: [],
      variables: [],
      comments: "",
      solutions: {},
      status: '',
    }
    this.updateSolution = this.updateSolution.bind(this)
    this.onSubmit = this.handleSubmit.bind(this)
  }

  UNSAFE_componentWillMount () {
    this.setState({
      expression: this.props.tokenize(this.props.data.exercise.expression),
      underlines: this.props.prepareUnderlines(this.props.data.modelSolution[0]),
      variables: this.props.data.variables,
      comments: this.props.data.exercise.comment,
      status: sessionStorage.getItem(Constant.MODE)
    })
  }
  componentDidMount(){
    this.setState({
      solutions: this.props.createSolutionsVariable(this.state.underlines.length),
    })
  }

  updateSolution (e) {
    this.setState({
      solutions: Object.assign({}, this.state.solutions, {[e.target.name]: e.target.value})
    })
  }

  async handleSubmit (e) {
    e.preventDefault()
    const userId = this.state.status == Constant.STUDENT ? this.props.data.studentId : 'NULL'
    for (let line = 0; line < this.state.underlines.length; line++) {
      await client.resource('expression_underline_values').create({
        exercise_id: this.props.data.exercise.id,
        student_id: userId,
        orderNo: line+1,
        value: this.props.cleanSolution(this.state.solutions["line"+line])
      })
    }
    if (this.state.status == Constant.STUDENT) {
      this.props.history.push('/expression-exercise/'+this.props.data.exercise.id+'/2/solution')
    }
    else { 
      sessionStorage.removeItem(Constant.MODE)
      this.props.history.push('/admin/exercise-list')
    }
  }

  render() {
    if (cookie.get(USER_ID_COOKIE_NAME) == 'null') {
      return (<NotLoggedIn />)
    }
    return (
      <form onSubmit={this.onSubmit}>
        <div className="header centerText">
          <h2><b>Part 2: Evaluate sub-expressions</b></h2>
          <p>In the given expression perform calculations for each underlined segment and write the final results in the underlines.</p>
          {this.state.variables.length != 0 ? <p>The variables are given above the expression.</p> : null }
        </div>
        <div className="exerciseRow">
          {this.state.comments ?
            <div className="box col-sm-6">
              <p><b>Comments from the teacher</b></p>
              {this.props.splitComment(this.state.comments).map((line, i) =>
                <p key={i}>{line}</p>
              )} 
              </div>
            : null
          }
        </div>
        <div className="exerciseRow">
          {this.state.variables.length != 0 ?
            <div className="box col-sm-6">
              <p><b>Variables</b></p>
              {this.state.variables.map((variab, i) => 
                <div>
                  <pre key={i}>{variab.variable}</pre>
                  <p/>
                </div>
              )}
            </div>
            :
            null
          }
        </div>
        <div className="exerciseRow">
          <div className="box expressionBoxWidth">
            <p><b>Expression</b></p>
            {this.state.expression.map((token, i) => 
              <ExpressionToken
                key={i}
                content={token}
                index={i}
              />
            )}
            {this.state.underlines.map((line, i) =>
                <Underline
                  key={i}
                  startIndex={line[0]}
                  endIndex={line[1]}
                  index={i}
                  status={Constant.PART_2}
                  labelStatus={Constant.PART_2}
                  updateInput={this.updateSolution}
                />
              )}
          </div>
        </div>
        <Submit />
      </form>
        )
    }
}

export { ExpressionExercisePart2 }

export default prepareData(ExpressionExercisePart2, { status: Constant.PART_2 })