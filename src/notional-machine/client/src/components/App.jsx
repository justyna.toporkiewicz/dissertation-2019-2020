import React from 'react'
import { HashRouter, Switch, Route, Redirect } from 'react-router-dom'
import client from './api-client'

/** Pages **/
import AdminExerciseList from './pages/AdminExerciseList'
import Err from './DefaultPages/Err'
import Exercise from './pages/Exercise'
import ExerciseList from './pages/ExerciseList'
import Home from './pages/Home'
import Signup from './pages/Signup'
import Login from './pages/Login'
import ExpressionTypeCreation from './ExerciseCreationPages/ExpressionTypeCreation'
import FlowTypeCreation from './ExerciseCreationPages/FlowTypeCreation'
import ExpressionExercisePage from './ExpressionExercisePages/ExpressionExercisePage'

/** App component - root **/
const App = () => (
  <HashRouter>
    <Switch>
      <Redirect from="/" exact to="/home/" />

      <Route exact path="/home" component={Home} />
      <Route exact path="/admin/exercise-list" component={AdminExerciseList} />
      <Route exact path="/exercise-list" component={ExerciseList} />
      <Route path="/exercise/:id" component={Exercise} />
      <Route path="/expression-exercise/:id" component={ExpressionExercisePage} />
      <Route exact path="/createFlowExercise" component={FlowTypeCreation} />
      <Route exact path="/createEvaluationExercise" component={ExpressionTypeCreation} />
      <Route exact path="/error" component={Err} />
      <Route exact path="/signup" component={Signup} />
      <Route exact path="/login" component={Login} />
    </Switch>
  </HashRouter>
)

export default App
