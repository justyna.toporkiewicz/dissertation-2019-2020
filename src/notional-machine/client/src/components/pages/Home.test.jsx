import React from 'react'
import { Router } from 'react-router-dom'
import { createMemoryHistory } from 'history'
import { render } from '@testing-library/react'
import Home from './Home'

describe("Home", () => {
    it("directs users towards logging in or signing up", () => {
        const history = createMemoryHistory()
        const { getByText } = render(
            <Router history={history}>
                <Home />
            </Router>
        )
        expect(getByText("Welcome to the Notional Machine Simulator and exercise creation tool")).toBeTruthy()
        expect(getByText("Log in")).toBeTruthy()
        expect(getByText("Sign up")).toBeTruthy()
    })
})