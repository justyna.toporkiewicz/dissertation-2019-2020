import React from 'react'
import { Router } from 'react-router-dom'
import { createMemoryHistory } from 'history'
import { render, fireEvent } from '@testing-library/react'
import LoginContainer from './Login'

describe("Login", () => {
    Object.defineProperty(window.document, "cookie", {
        set: ()=>({})
    })
    it("has all correct elements", () => {
        const history = createMemoryHistory()
        const { getByText } = render(
            <Router history={history}>
                <LoginContainer />
            </Router>
        )
        expect(getByText("Log In as a student")).toBeTruthy()
        expect(getByText("Admin Log In")).toBeTruthy()
        expect(getByText("No account yet?")).toBeTruthy()
        expect(getByText("Log In Using Facebook")).toBeTruthy()
    })

    it("switches modes", () => {
        const history = createMemoryHistory()
        const { getByText } = render(
            <Router history={history}>
                <LoginContainer />
            </Router>
        )
        fireEvent.click(getByText("Admin Log In"))
        expect(getByText("Log In as an admin")).toBeTruthy()
        fireEvent.click(getByText("Student Log In"))
        expect(getByText("Log In as a student")).toBeTruthy()
    })
})