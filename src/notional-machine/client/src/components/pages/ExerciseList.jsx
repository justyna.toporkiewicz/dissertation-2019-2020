import React from 'react'
import { Link } from 'react-router-dom'
import client from '../api-client'
import NotLoggedIn from '../DefaultPages/NotLoggedIn'
import { USER_ID_COOKIE_NAME } from '../config'
import * as cookie from '../cookies'
import * as Constant from '../Constants'

function formatDate (dateString) {
  const [y, m, d] = dateString.replace(/T.*$/, '').split('-')
  return [d, m, y].join('/')
}

function getUniqueCompleted (completed) {
  let uniqueList = []
  for (let entry = 0; entry < completed.length; entry++) {
    if (uniqueList.includes(completed[entry].exercise_id)){
      continue
    }
    uniqueList = uniqueList.concat([completed[entry].exercise_id])
  }
  return uniqueList
}

/** Represents the exercise list a pupil sees **/
const ExerciseList = ({ flowExercises, evaluationExercises, completedExercises, startExercise, onLogOut }) => (
  <div className="ExerciseList container">
    <Link className="btn btn-outline-danger LogOutButton" onClick={onLogOut} to="/home">Log Out</Link>
    <h1>Available exercises:</h1>
    <hr></hr>
    <h2>Flow Exercises</h2>
    <table className="table table-striped">
      <thead>
         <tr>
           <td> ID </td>
           <td> Exercise Name </td>
           <td> Date Added </td>
           <td colSpan={3}> Exercise Sections </td>
         </tr>
      </thead>
      <tbody>
        {flowExercises.map(({ id, name, createdAt }) =>
          <tr key={id}>
            <th> {id} </th>
            <td> {name} </td>
            <td> Added on: {formatDate(createdAt)} </td>
            <td><Link to={'/exercise/' + id + '/task/1' }> Expr </Link></td>
            <td><Link to={'/exercise/' + id + '/task/2' }> Flow </Link></td>
            <td><Link to={'/exercise/' + id + '/task/3' }> Exec </Link></td>
          </tr>
        )}
      </tbody>
    </table>
    <hr></hr>
    <h2>Expression Exercises</h2>
    <table className="table table-striped">
      <thead>
         <tr>
           <td> ID </td>
           <td> Exercise Name </td>
           <td> Date Added </td>
           <td colSpan={2}> Exercise Sections </td>
           <td> Status </td>
         </tr>
      </thead>
      <tbody>
        {evaluationExercises.map(({ id, name, createdAt }) =>
          <tr key={id}>
            <th> {id} </th>
            <td> {name} </td>
            <td> Added on: {formatDate(createdAt)} </td>
            <td><div className="btn btn-success" onClick={() => startExercise('/expression-exercise/' + id + '/1', Constant.STUDENT)}> Start </div></td>
            <td><div className="btn btn-outline-success" onClick={() => startExercise('/expression-exercise/' + id + '/2', Constant.STUDENT)}> Start from Part 2 </div></td>
            <td>{completedExercises.includes(id+"") ? <div className="exerciseStatus">&#10003;</div> : <div className="exerciseStatus">&#9711;</div>}</td>
          </tr>
        )}
      </tbody>
    </table>
  </div>
)

/** Displays the exercise list **/
class ExerciseListContainer extends React.Component {

  constructor () {
    super()
    this.state = { flowExercises: [], evaluationExercises: [], completedExercises: [] }
  }

  async componentDidMount () {
    sessionStorage.setItem('studentId', cookie.get(USER_ID_COOKIE_NAME))
    const flowExercises = await client.resource('exercise').find()
    const evaluationExercises = await client.resource('expression_exercise').find()
    const completed = await client.resource('expression_underline').find({ student_id: cookie.get(USER_ID_COOKIE_NAME)})
    this.setState({ flowExercises, evaluationExercises, completedExercises: getUniqueCompleted(completed) })
  }
  
  startExercise (url, status){
    sessionStorage.setItem('status', status)
    this.props.history.push(url)
  }

  handleLogOut () {
    cookie.set(USER_ID_COOKIE_NAME, 'null')
  }

  render () {
    if(cookie.get(USER_ID_COOKIE_NAME) != 'null' && cookie.get(USER_ID_COOKIE_NAME) != 'NULL') {
      return (<ExerciseList {...this.state} onLogOut={this.handleLogOut.bind(this)} startExercise={this.startExercise.bind(this)} />)
    }
    return (<NotLoggedIn />)
  }
}

export default ExerciseListContainer
