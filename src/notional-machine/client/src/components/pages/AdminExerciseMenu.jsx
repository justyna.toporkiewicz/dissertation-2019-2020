import React from 'react'
import * as Constant from '../Constants'

const AdminExerciseMenu = ({id, exerciseType, deleteExercise, attemptToggle, reviewExercise}) => {
    return (
        <div className="btn-group-vertical">
            {exerciseType == Constant.EXPRESSION_EVALUATION ?
                <div 
                    onClick={() => reviewExercise('/expression-exercise/' + id + '/1', Constant.TEACHER_UPDATE)}
                    className="btn btn-success">
                        Update exercise solution
                </div> : null
            }
            <div
                onClick={() => attemptToggle(id, exerciseType)}
                className="btn btn-secondary">
                    Attempted by
                </div>
            <div
                onClick={() => deleteExercise(id, exerciseType)}
                className="btn btn-danger">
                    Delete Exercise
                </div>
            {exerciseType == Constant.EXPRESSION_EVALUATION ?
                <div
                    onClick={() => reviewExercise('/expression-exercise/' + id + '/2/solution', Constant.TEACHER_VIEW)}
                    className="btn btn-success">
                        See Exercise
                </div> : null
            }
        </div>
    )
}

export default AdminExerciseMenu