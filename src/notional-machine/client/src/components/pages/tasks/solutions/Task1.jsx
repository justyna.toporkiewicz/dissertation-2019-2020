import React from 'react'
import uniqBy from 'lodash/uniqBy'
import clonedeep from 'lodash.clonedeep'
import hydrate from '../hydrate'
import Task1Layout from '../../../layout/Task1Layout'

/** Checks whether two expressions are equal **/
function equals(e1, e2) {
    return e1.line === e2.line && e1.start_pos === e2.start_pos && e1.end_pos === e2.end_pos
}

/** Set up a unique key for a new expression **/
function getExpressionUniqueKey({line, start_pos, end_pos}) {
    return [line, start_pos, end_pos].join(':')
}

/** Sorts the expressions by positions in the code fragment **/
function sort(expressions) {
    return expressions.slice().sort((a, b) => (a.start_pos < b.start_pos) ? -1 : 1)
}

/** Create a new expression **/
function newExpression(line, start, end, kind) {
    return {
        id: getExpressionUniqueKey(line, start, end),
        line: line,
        start_pos: start,
        end_pos: end,
        kind: kind
    }
}

/** Given an expression and details, update the expression contents in place **/
function updateExp(expr, line, start, end, kind) {
    expr.id = getExpressionUniqueKey(line, start, end);
    expr.line = line;
    expr.start_pos = start;
    expr.end_pos = end;
    expr.kind = kind
}

/** exprs is all attempted and correct expressions
 *  Check them against each other, breaking into parts as necessary, marking as (A)ttempt, (C)orrect, (S)olution **/
function splitMarkExpressions(atts, sols) {
    let attempts = clonedeep(atts);
    let solutions = clonedeep(sols);
    sort(attempts);
    sort(solutions);
    // A tag is added to each solution and attempt.  Attempts are easy - they're "a"
    // The solutions are initially marked as "x" - this means they're unprocessed.  At the point where it is
    // known that they have been incorporated into the newExprs list (see below), the id is set to "".
    // If a solution is broken in two - because a part is correct (matched with an attempt) and a part is left
    // over, then the solution is reduced in size, in place in the solutions list - then
    // if the left over part is BEFORE the correct part, the reduced sol is marked with "s" - because of the
    // ordering of the parts, it cannot now be part of an attempt
    // if the left over part is AFTER the correct part, the reduced sol is marked with "x" - another attempt might
    // overlap with it.
    // See how, when looping over the solutions, if the id is "" or the tag (kind) is "s", they're ignored
    // If an attempt is partially correct, but an unchecked part comes after the correct part, then a new attempt
    // fragment is spliced into the list just after the current position, and immediately checked next time around.
    // Once all attempt fragments have been checked, the solutions list is checked.  Any solution NOT with id ""
    // (and so actually marked "s") is copied into newExprs as a missed part of the solution.
    solutions.map( (s)=>{ s.kind="x"});
    attempts.map( (a)=>{ a.kind="a"})
    let newExprs = [];  // accumulates all the new expression fragments, once identified
    let aIndex = 0;
    while (aIndex < attempts.length) {
        let att = attempts[aIndex];
        let sIndex = 0;
        let attProcessed = false;
        while (sIndex < solutions.length && !attProcessed) {
            // Assume that we will process the attempt this time
            attProcessed = true;

            //find out what kind of overlap it is
            let sol = solutions[sIndex];
            // we can skip over many of the solutions, if already processed but left in the list
            if (att.line === sol.line && sol.id !== "" && sol.kind !== "s") {
                if (att.start_pos < sol.start_pos) {
                    if (att.end_pos >= sol.start_pos) {
                        if (att.end_pos <= sol.end_pos) {
                            console.log("we have an attempt fragment at start, then a correct part, then maybe an unresolved sol part ");
                            let attfrag = newExpression(att.line, att.start_pos, sol.start_pos - 1, "a");
                            let corrfrag = newExpression(att.line, sol.start_pos, att.end_pos, "c");
                            newExprs.push(attfrag, corrfrag);
                            if (att.end_pos < sol.end_pos) {
                                updateExp(sol, sol.line, att.end_pos + 1, sol.end_pos, "x")// overwrite the sol to reduce size
                            } else {
                                // The solution was completely covered by the attempt - mark the sol as done.
                                sol.id = ""
                            }
                        } else {
                            console.log("we have an attempt fragment at start, the solution is all correct, then an unresolved att part ");
                            let attfrag = newExpression(att.line, att.start_pos, sol.start_pos - 1, "a");
                            let corrfrag = newExpression(att.line, sol.start_pos, sol.end_pos, "c");
                            newExprs.push(attfrag, corrfrag);
                            sol.id=""
                            // Add the unresolved attribute part onto the end of the att array
                            let unresolvedattfrag = newExpression(att.line, sol.end_pos + 1, att.end_pos, "a");
                            attempts.splice(aIndex+1, 0, unresolvedattfrag);
                        }
                    } else {
                        console.log("The attempt is fully before this solution.  Add it in to the newExprs");
                        newExprs.push(newExpression(att.line, att.start_pos, att.end_pos, "a"))
                        //sIndex = attempts.length; // and avoid testing against any more solutions
                    }
                } else if (att.start_pos === sol.start_pos) {
                    if (att.end_pos <= sol.end_pos) {
                        console.log("we have a correct fragment and then maybe an unresolved solution fragment ");
                        newExprs.push(newExpression(att.line, sol.start_pos, att.end_pos, "c"));
                        // overwrite the sol to reduce size if it's only a partially correct attempt
                        if (att.end_pos < sol.end_pos) {
                            updateExp(sol, sol.line, att.end_pos + 1, sol.end_pos, "x")
                        } else {
                            sol.id = ""
                        }
                    } else {
                        console.log("we have a full correct part - the solution - and then an unresolved att part ");
                        let corrfrag = newExpression(att.line, sol.start_pos, att.end_pos, "c");
                        newExprs.push(attfrag, corrfrag);
                        let unresolvedattfrag = newExpression(att.line, sol.end_pos + 1, att.end_pos, "a");
                        attempts.splice(aIndex+1, 0, unresolvedattfrag);
                        // mark the solution as spent - move across to the newExprs
                        sol.id = ""
                    }
                } else if (att.start_pos < sol.end_pos) {
                    if (att.end_pos <= sol.end_pos) {
                        console.log("a sol part at start, a correct part in middle, and possibly an unresolved sol part at end ");
                        let solFrag = newExpression(sol.line, sol.start_pos, att.start_pos - 1, "s");
                        let corrFrag = newExpression(att.line, att.start_pos, att.end_pos, "c");
                        newExprs.push(solFrag, corrFrag);
                        // overwrite the sol to reduce size
                        if (att.end_pos < sol.end_pos) {
                            updateExp(sol, sol.line, att.end_pos + 1, sol.end_pos, "x")
                        }
                    } else {
                        console.log("a sol part at start, the rest of the sol is all correct, then an unresolved att part at end ");
                        let corrFrag = newExpression(att.line, att.start_pos, sol.end_pos, "c");
                        newExprs.push(corrFrag);
                        let unresolvedattfrag = newExpression(att.line, sol.end_pos + 1, att.end_pos, "a");
                        attempts.splice(aIndex+1, 0, unresolvedattfrag);
                        updateExp(sol, sol.line, sol.start_pos, att.start_pos - 1, "s")
                        // this one is definitely wrong - the sol part at the start could be put straight into
                        // the newExprs.  The the updated sol bit would be the bit after the attempt.
                    }
                } else if (att.start_pos === sol.end_pos) {
                    if (att.end_pos === sol.end_pos) {
                        console.log("a soln fragment, then a one char correct part is all ");
                        let corrFrag = newExpression(att.line, att.start_pos, att.end_pos, "c");
                        newExprs.push(corrFrag);
                        updateExpr(sol, sol.line, sol.start_pos, att.end_pos - 1, "s")
                    } else {
                        console.log("a soln fragment, a one char correct part, and then an unresolved att part coming after ");
                        let corrFrag = newExpression(att.line, att.start_pos, sol.end_pos, "c");
                        newExprs.push(corrFrag);
                        let unresolvedattfrag = newExpression(att.line, sol.end_pos + 1, att.end_pos, "a");
                        attempts.splice(aIndex+1, 0, unresolvedattfrag);
                        updateExp(sol, sol.line, sol.start_pos, att.start_pos - 1, "s")
                    }
                } else {
                    console.log("The attempt comes completely after this solution");
                    sol.kind = "s";
                    attProcessed = false
                }
            } else {
                // Different lines - attempt must be on the earlier line - hence convert to attempt
                if( att.line != sol.line ){
                    console.log("The attempt and solution are on different lines")

                    if( att.line < sol.line ){
                        // The att comes first.  There can be no overlaps with other sols, so we're done with this att
                        newExprs.push(newExpression(att.line, att.start_pos, att.end_pos, "a"))
                    } else {
                        // This att comes after the sol.  It could still overlap with later sols - not processed yet
                        sol.kind = "s"
                        attProcessed = false
                    }
                } else {
                    console.log( "An added att, after all other solns" )
                    newExprs.push( newExpression( att.line, att.start_pos, att.end_pos, "a" ) )
                }
            }
            sIndex++
        }

        if( !attProcessed ){
            console.log( "An attempt is after all solution fragments - add it in" );
            let attFrag = newExpression(att.line, att.start_pos, att.end_pos, "a");
            newExprs.push( attFrag );

        }
        aIndex++
    }


    // Now we scan through what's left in the solutions part
    var sIndex;
    for (sIndex = 0; sIndex < solutions.length; sIndex++) {
        let sol = solutions[sIndex];
        if (sol.id !== "") {
            let solFrag = newExpression(sol.line, sol.start_pos, sol.end_pos, "s");
            newExprs.push(solFrag)
        }
    }
    console.log( newExprs );

    return sort( newExprs )
}

    /** Represents the solution of the first task **/
    class Task1 extends React.Component {

        /** Get a specific expression **/
        findExpression(id) {
            let expression = this.props.data.expressions.find(expression => expression.id === id);
            if (!expression) {
                expression = this.props.data.model.expressions.find(expression => expression.id === id)
            }
            return expression
        }

        /** Check if the expression exists **/
        hasEqual(id, expressions) {
            const expression = this.findExpression(id);
            return !!expressions.find(equals.bind(this, expression))
        }

        /** Check if the expression is featured in the model solution **/
        isCorrect(id) {
            return this.hasEqual(id, this.props.data.model.expressions) && this.hasEqual(id, this.props.data.expressions)
        }

        /** Check if the expression is not part of the model solution **/
        isIncorrect(id) {
            return !this.hasEqual(id, this.props.data.model.expressions)
        }

        /** Check if an expression is part of the model solution but was not selected by the user **/
        isMissed(id) {
            return !this.hasEqual(id, this.props.data.expressions)
        }

        render() {
            /** let expressions = this.props.data.expressions.concat(this.props.data.model.expressions **/
            /** expressions = uniqBy(expressions, getExpressionUniqueKey) **/
            let attempts = this.props.data.expressions;
            let solutions = this.props.data.model.expressions;
            let expressions = splitMarkExpressions(attempts, solutions);

            return (
                <Task1Layout
                    {...this.props}
                    checking={true}
                    data={Object.assign({}, this.props.data, {expressions})}
                    expression={{
                        correct: this.isCorrect.bind(this),
                        incorrect: this.isIncorrect.bind(this),
                        missed: this.isMissed.bind(this)
                    }}
                    next={{to: '../2', text: 'Next task'}}
                />
            )
        }
    }

    export default hydrate(Task1)
