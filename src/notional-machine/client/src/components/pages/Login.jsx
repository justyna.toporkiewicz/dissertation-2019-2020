import React from 'react'
import client from '../api-client'
import * as cookie from '../cookies'
import { USER_ID_COOKIE_NAME } from '../config'
import { Link } from 'react-router-dom'
import * as Constant from '../Constants'

/** Login page for pupils **/
const Login = ({ username, password, alert, mode, onChange, onLogin, onChangeModes, onTeacherLogin }) => (
  <div className="Login container">
    {alert ? <div className="alert alert-danger col-sm-5 centerButton">Incorrect Log In</div> : null}
    <div className="btn btn-outline-success centerButton" onClick={onChangeModes}>{mode == Constant.STUDENT ? "Admin Log In" : "Student Log In" }</div>
    <h1 className="centerText">Log In {mode == Constant.STUDENT ? "as a student" : "as an admin"}</h1>
    <div className="row">
      <div className="col-md-4 offset-md-4">
        <form className="form" onSubmit={mode == Constant.STUDENT ? onLogin : onTeacherLogin}>
          <div className="form-group">
            <input
              className="form-control"
              type="text"
              placeholder="Username"
              name="username"
              value={username}
              onChange={onChange} />
          </div>
          <div className="form-group">
            <input
              className="form-control"
              type="password"
              placeholder="Password"
              name="password"
              value={password}
              onChange={onChange}
            />
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-primary btn-block">Log in</button>
          </div>
          <Link to='/signup'>No account yet?</Link>
        </form>
      </div>
    </div>
    <hr className="col-md-5"></hr>
    <div className="row">
      <div className="btn btn-primary centerButton disabled">Log In Using Facebook</div>
    </div>
  </div>
)

/** Adds the functionality to the login page and visualises it **/
class LoginContainer extends React.Component {

  constructor () {
    super()
    this.state = { username: '', password: '', alert: false, mode: Constant.STUDENT }
  }

  /** consume a change **/
  handleChange (e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  /** log a pupil in **/
  async handleLogin (e) {
    e.preventDefault()
    try {
      const [pupil] = await client.resource('pupil').find({username: this.state.username, password: this.state.password})
      if (!pupil) {
        this.setState({ username: '', password: '', alert: true})
        return
      }
      cookie.set(USER_ID_COOKIE_NAME, pupil.id)
      this.props.history.push('/exercise-list')
    } catch (error) {
      console.log(error)
      this.props.history.push('/error')
    }
  }

  handleSwitchModes () {
    if(this.state.mode == Constant.STUDENT){
      this.setState({
        mode: Constant.TEACHER
      })
    }
    else {
      this.setState({
        mode: Constant.STUDENT
      })
    }
  }

  async handleTeacherLogin (e) {
    e.preventDefault()
    cookie.set(USER_ID_COOKIE_NAME, 'NULL')
    this.props.history.push('/admin/exercise-list')
  }
  
  render () {
    return (<Login {...this.state} onChange={this.handleChange.bind(this)} onLogin={this.handleLogin.bind(this)} onChangeModes={this.handleSwitchModes.bind(this)} onTeacherLogin={this.handleTeacherLogin.bind(this)} />)
  }
}

export default LoginContainer
