import React from 'react'
import * as Constant from '../Constants'
import classnames from 'classnames'
import AdminExerciseMenu from './AdminExerciseMenu'
import './AdminEList.css'

function formatDate (dateString) {
    const [y, m, d] = dateString.replace(/T.*$/, '').split('-')
    return [d, m, y].join('/')
  }  

const AdminEList = ({
    exerciseType,
    exercises,
    expanded,
    reviewExercise,
    attempted,
    fetching,
    onToggle,
    onDelete,
    onHover,
    showHover,
    showExerciseMenu,
    handleShowMenu,
    seeStudentsSolution
}) => (
    <div>
        <h2>{exerciseType == Constant.PROGRAM_FLOW ? "Flow type exercises" : "Expression type exercises"}</h2>
        <table className="table table-striped">
        <tbody>
            {exercises.map(({ id, name, expression, createdAt }) =>
            <tr  onMouseEnter={() => onHover(id)} onMouseLeave={() => onHover(id)} key={id}>
                <th> {id} </th>
                <td>
                    <div>{name}</div>
                    {showHover == id ? <div className="hoverExplanation">{expression}</div> : null}
                </td>
                <td> Added on {formatDate(createdAt)} </td>
                <td className="text-right">
                {showExerciseMenu != exerciseType + " " + id ? 
                <div className="btn btn-success" onClick={() => handleShowMenu(exerciseType, id)}>Exercise Menu</div>
                : <AdminExerciseMenu
                    id={id}
                    exerciseType={exerciseType}
                    deleteExercise={onDelete}
                    attemptToggle={onToggle}
                    reviewExercise={reviewExercise}
                    />
                }
                <div className={classnames('attempted-by', { collapse: expanded !== id })}>
                    {fetching ? <p>Loading...</p> : attempted.map(pupil =>
                    <div className="btn btn-outline-secondary" key={pupil.id} onClick={() => seeStudentsSolution(id, pupil.id)}>{pupil.username}</div>
                    )}
                </div>
                </td>
            </tr>
            )}
        </tbody>
        </table>
    </div>
)

export default AdminEList