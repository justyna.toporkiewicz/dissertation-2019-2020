import React from 'react'
import { Link } from 'react-router-dom'

/** Home page - currently not used **/
class Home extends React.Component {
  render() {
    return (
    <div className="Home container centerText col-sm-8">
    <h1>Notional Machine Simulator</h1>
    <p>Welcome to the Notional Machine Simulator and exercise creation tool</p>
    <p>To access the exercises Log In</p>
    <p>If you don't have an account yet create one by clicking Sign Up</p>
    <div className="btn btn-light btn-lg centerButton" onClick={() => this.props.history.push('/login')}>Log in</div>
    <div className="btn btn-light btn-lg centerButton" onClick={() => this.props.history.push('/signup')}>Sign up</div>
  </div>
    )}
}

export default Home
