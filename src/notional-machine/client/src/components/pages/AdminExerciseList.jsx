import React from 'react'
import { Link } from 'react-router-dom'
import { USER_ID_COOKIE_NAME } from '../config'
import client from '../api-client'
import NotLoggedIn from '../DefaultPages/NotLoggedIn'
import CodeFlowButton from '../ExerciseCreationPages/CodeFlowButton'
import AdminEList from './AdminEList'
import DeletionWarning from '../DefaultPages/DeletionWarning'
import ExpressionEvaluationButton from '../ExerciseCreationPages/ExpressionEvaluationButton'
import * as Constant from '../Constants'
import * as cookie from'../cookies'

/** Represents the list of exercises the admin sees **/
const AdminExerciseList = ({
  flowExercises,
  evaluationExercises,
  expanded,
  expandedTypeTwo,
  reviewExercise,
  attempted,
  attemptedTypeTwo,
  fetching,
  onToggle,
  onDelete,
  onHover,
  showDeleteWarning,
  deleteConfirmation,
  createFlowExercise,
  createEvaulationExercise,
  onLogOut,
  showHover,
  showExerciseMenu,
  handleShowMenu,
  seeStudentsSolution }) => (
  <div>
    <DeletionWarning showDeleteWarning={showDeleteWarning} onDelete={onDelete} deleteConfirmation={deleteConfirmation}/>
    <div className="AdminExerciseList container">
      <Link className="btn btn-outline-danger LogOutButton" onClick={onLogOut} to="/home">Log Out</Link>
      <h1>Admin - List of Exercises</h1>
      <hr></hr>
      <AdminEList
        exerciseType={Constant.PROGRAM_FLOW}
        exercises={flowExercises}
        expanded={expanded}
        reviewExercise={reviewExercise}
        attempted={attempted}
        fetching={fetching}
        onToggle={onToggle}
        onDelete={onDelete}
        onHover={() => ({})}
        showHover={showHover}
        showExerciseMenu={showExerciseMenu}
        handleShowMenu={handleShowMenu}
        seeStudentsSolution={seeStudentsSolution}
      />
      <hr></hr>
      <AdminEList
        exerciseType={Constant.EXPRESSION_EVALUATION}
        exercises={evaluationExercises}
        expanded={expandedTypeTwo}
        reviewExercise={reviewExercise}
        attempted={attemptedTypeTwo}
        fetching={fetching}
        onToggle={onToggle}
        onDelete={onDelete}
        onHover={onHover}
        showHover={showHover}
        showExerciseMenu={showExerciseMenu}
        handleShowMenu={handleShowMenu}
        seeStudentsSolution={seeStudentsSolution}
      />
      <hr></hr>
      <div className="col-sm-12" style={{position: 'inherit'}}>
        <h1>Create</h1>
        <CodeFlowButton onClick={createFlowExercise} />
        <ExpressionEvaluationButton onClick={createEvaulationExercise} />
      </div>
    </div>
  </div>
)

/** Adds the functionality to the ExerciseList component **/
class AdminExerciseListContainer extends React.Component {

  constructor () {
    super()
    this.state = {
      flowExercises: [],
      evaluationExercises: [],
      attempted: [],
      attemptedTypeTwo: [],
      expanded: null,
      expandedTypeTwo: null,
      showExerciseMenu: [],
      fetching: false,
      values: { name: '', code_fragment: '' },
      showHover: -1,
      showDeleteWarning: {id: '', exercise_type: ''},
    }
    this.closeMenu = this.closeMenu.bind(this)
  }

  /** Fetches all exercises from the server **/
  async fetchExercises () {
    const flowExercises = await client.resource('exercise').find()
    const evaluationExercises = await client.resource('expression_exercise').find()
    this.setState({ flowExercises, evaluationExercises })
  }

  componentDidMount () {
    this.fetchExercises()
  }

  handleHover (index) {
    if (this.state.showHover == index) this.setState({showHover: -1})
    else {this.setState({showHover: index})}
  }

  handleShowMenu (type, id) {
    this.setState({showExerciseMenu: type + " " + id}, () => {
      document.addEventListener('click', this.closeMenu);
    })
  }

  closeMenu() {
    this.setState({ showExerciseMenu: [] }, () => {
      document.removeEventListener('click', this.closeMenu);
    })
  }

  /** Get the pupils that have attempted an exercise **/
  async handleToggle (id, exercise_type) {
    let attempted = []
    switch (exercise_type) {
      case Constant.PROGRAM_FLOW:
        if (this.state.expanded === id) {
          this.setState({ expanded: null })
          return
        }
        this.setState({ expanded: id, fetching: true })
        const solutions = await client.resource('solution').find({ exercise_ID: id })
        for (const solution of solutions) {
          if (solution.pupil_ID) {
            const pupils = await client.resource('pupil').find({ id: solution.pupil_ID })
            attempted = [...attempted, ...pupils]
          }
        }
        this.setState({ fetching: false, attempted })
        break
      case Constant.EXPRESSION_EVALUATION:
        if (this.state.expandedTypeTwo === id) {
          this.setState({expandedTypeTwo: null})
          return
        }
        this.setState({expandedTypeTwo: id, fetching: true })
        const pupils = await client.resource('expression_underline').find({ exercise_id: id })
        let unique = {}
        for (const pupil of pupils) {
          if(pupil.student_id && pupil.student_id != 'NULL' && !unique[pupil.student_id]) {
            const found = await client.resource('pupil').find({ id: pupil.student_id })
            attempted = attempted.concat(...found)
            unique[pupil.student_id] = true
          }
        }
        this.setState({ fetching: false, attemptedTypeTwo: attempted})
    }

  }

  /** Deletes an exercise **/
  async deleteConfirmation (id, exercise_type) {
    this.setState({showDeleteWarning: {id: '', exercise_type: ''}})
    switch (exercise_type) {
      case Constant.PROGRAM_FLOW:
        await client.resource('exercise').remove(id)
        this.fetchExercises()
        break
      case Constant.EXPRESSION_EVALUATION:
        await client.resource('expression_exercise').remove(id)
        const underlines = await client.resource('expression_underline').find({ exercise_id: id })
        const values = await client.resource('expression_underline_values').find({ exercise_id: id })
        const variables = await client.resource('expression_variable').find({ exercise_id: id })
        for (const under of underlines) {
          if (under.id) {
            await client.resource('expression_underline').remove(id = under.id)
          }
        }
        for (const valu of values) {
          if (valu.id) {
            await client.resource('expression_underline_values').remove(id = valu.id)
          }
        }
        for (const variab of variables) {
          if (variab.id) {
            await client.resource('expression_variable').remove(id = variab.id)
          }
        }
        this.fetchExercises()
    }
  }

  handleDelete (id, exercise_type) {
    this.setState({showDeleteWarning: {id, exercise_type}})
  }

  createFlowExercise () {
    this.props.history.push('/createFlowExercise')
  }

  createEvaulationExercise () {
    this.props.history.push('/createEvaluationExercise')
  }

  reviewExercise (url, status){
    sessionStorage.setItem('status', status)
    this.props.history.push(url)
  }

  handleLogOut () {
    cookie.set(USER_ID_COOKIE_NAME, null)
  }

  seeStudentsSolution (exercise_id, student_id){
    sessionStorage.setItem('studentId', student_id)
    sessionStorage.setItem('status', Constant.TEACHER_STUDENT_SOLUTION)
    this.props.history.push('/expression-exercise/' + exercise_id + '/2/solution')
  }

  render () {
    if (cookie.get(USER_ID_COOKIE_NAME) == 'NULL') {
      return (
        <AdminExerciseList
          {...this.state}
          onToggle={this.handleToggle.bind(this)}
          onDelete={this.handleDelete.bind(this)}
          deleteConfirmation={this.deleteConfirmation.bind(this)}
          createFlowExercise={this.createFlowExercise.bind(this)}
          createEvaulationExercise={this.createEvaulationExercise.bind(this)}
          reviewExercise={this.reviewExercise.bind(this)}
          onLogOut={this.handleLogOut.bind(this)}
          onHover={this.handleHover.bind(this)}
          handleShowMenu={this.handleShowMenu.bind(this)}
          seeStudentsSolution={this.seeStudentsSolution.bind(this)}
        />
      )
    }
  return (<NotLoggedIn />)
  }
}

export default AdminExerciseListContainer
