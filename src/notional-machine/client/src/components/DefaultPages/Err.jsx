import React from 'react'
import './Loading.css'

/** Displays an error message **/
const Err = () => (
    <div className="loading">
        An unexpected error occurred. Please, refresh the page.
    </div>
)

export default Err
