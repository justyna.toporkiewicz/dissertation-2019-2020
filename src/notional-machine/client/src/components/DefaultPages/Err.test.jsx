import React from 'react'
import {render} from '@testing-library/react'
import Err from './Err'

describe("Err", () => {
    it("has the correct text", () => {
        const { getByText } = render(<Err/>)
        expect(getByText("An unexpected error occurred. Please, refresh the page.")).toBeTruthy()
    })
})