import React from 'react'
import './DeletionWarning.css'

const DeletionWarning = ({showDeleteWarning, onDelete, deleteConfirmation}) => {
    let id = showDeleteWarning.id
    let exerciseType = showDeleteWarning.exercise_type
    if (!(showDeleteWarning.id == '')) {
        return(
            <div className="grey" style={{height: document.body.scrollHeight+'px'}}>
                <div className="bodyDeletion">
                    <h4>Are you sure you want to delete this exercise?</h4>
                    <p><b>This action is irreversable!</b></p>
                    <p><div className="centerbutton btn btn-danger" onClick={() => deleteConfirmation(id, exerciseType)}>Yes, delete the exercise</div></p>
                    <p><div className="centerbutton btn btn-danger" onClick={() => onDelete('', '')}>No, keep the exercise</div></p>
                </div>
            </div>
        )
    }
    else {
        return null
    }
}

export default DeletionWarning