import React from 'react'
import {render} from '@testing-library/react'
import Loading from './Loading'

describe("Loading", () => {
    it("has the correct text", () => {
        const { getByText } = render(<Loading/>)
        expect(getByText("Loading ...")).toBeTruthy()
    })
})