import React from 'react'
import "./NotLoggedIn.css"
import { Link } from 'react-router-dom'

const NotLoggedIn = ({}) => (
    <div className="notloggedin">
        <h4>You cannot view this page until you have logged in</h4>
        <div className="centerbutton"><Link className="btn btn-danger" to='/login'>Log In</Link></div>
        <div className="centerbutton"><Link className="btn btn-danger" to="/signup">Create an account</Link></div>
    </div>
)

export default NotLoggedIn