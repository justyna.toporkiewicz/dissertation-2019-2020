import React from 'react'
import {render, fireEvent} from '@testing-library/react'
import DeletionWarning from './DeletionWarning'

describe("DeletionWarning", () => {
    it("doesn't show up if there is no id and exercise type passed", () => {
        let exercise = {id: '', exercise_type: ''}
        const { queryByText } = render(<DeletionWarning
            showDeleteWarning={exercise}
            onDelete={() => ({})}
            deleteConfirmation={() => ({})}/>)
        expect(queryByText("Are you sure you want to delete this exercise?")).toBeFalsy()
    })

    it("renders all the information when given id and exercise type", () => {
        let exercise = {id: '1', exercise_type: 'ExpressionExercise'}
        const { getByText } = render(<DeletionWarning
            showDeleteWarning={exercise}
            onDelete={() => ({})}
            deleteConfirmation={() => ({})}/>)
        expect(getByText("Are you sure you want to delete this exercise?")).toBeTruthy()
        expect(getByText("This action is irreversable!")).toBeTruthy()
        expect(getByText("Yes, delete the exercise")).toBeTruthy()
        expect(getByText("No, keep the exercise")).toBeTruthy()
    })

    it("buttons are clickable", () => {
        let exercise = {id: '1', exercise_type: 'ExpressionExercise'}
        const { getByText } = render(<DeletionWarning
            showDeleteWarning={exercise}
            onDelete={() => ({})}
            deleteConfirmation={() => ({})}/>)
        expect(getByText("Are you sure you want to delete this exercise?")).toBeTruthy()
        fireEvent.click(getByText("Yes, delete the exercise"))
        fireEvent.click(getByText("No, keep the exercise"))
    })
})