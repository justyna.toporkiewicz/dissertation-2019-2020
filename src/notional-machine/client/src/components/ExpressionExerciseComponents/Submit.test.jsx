import React from 'react'
import { render } from '@testing-library/react'
import Submit from './Submit'

describe("Submit", () => {
    it("has default text", async () => {
        const { getByText } = render(<Submit />)
        expect(getByText("Submit")).toBeTruthy()
    })

    it("has given text", async () => {
        const { getByText } = render(<Submit value="test" />)
        expect(getByText("test")).toBeTruthy()
    })
})