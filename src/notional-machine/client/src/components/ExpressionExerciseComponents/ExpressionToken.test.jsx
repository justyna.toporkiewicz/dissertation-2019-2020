import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import ExpressionToken from './ExpressionToken'

const mockUnderline = jest.fn(() => "UNDERLINE")

describe("ExpressionToken", () => {
    it("renders with the given token", async () => {
        const { getByText } = render(<ExpressionToken content="a" index={1} />)
        fireEvent.click(getByText("a"))
        expect(getByText("a")).toBeTruthy()
    })

    it("responds to clicks", async () => {
        const { getByText } = render(<ExpressionToken
            content="a"
            index={1}
            underlineFunction={mockUnderline} />)
        fireEvent.click(getByText("a"))
        expect(mockUnderline.mock.calls.length).toBe(1)
    })

    it("changeColor and hoverChange don't break the component", async () => {
        const { getByText } = render(<ExpressionToken
            content="a"
            index={1}
            changeColor={true}
            hoverChange={true} />)
        expect(getByText("a")).toBeTruthy()
    })
})