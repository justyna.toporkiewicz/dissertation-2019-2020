import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import UnderlineMenu from './UnderlineMenu'

const mockMove = jest.fn((id, up) => up ? "MOVE UP" : "MOVE DOWN")
const mockDelete = jest.fn((id) => "DELETE")

describe("UnderlineMenu", () => {
    it("has delete button", async () => {
        const { getByText } = render(
            <UnderlineMenu
                deleteFunction={() => ({})}
                moveFunction={() => ({})}
                id={0}
                listLength={1}
            />
        )
        expect(getByText("Delete")).toBeTruthy()
    })

    it("move up is invisible", async () => {
        const { queryByText } = render(
            <UnderlineMenu
                deleteFunction={() => ({})}
                moveFunction={() => ({})}
                id={0}
                listLength={1}
            />
        )
        expect(queryByText("Move Up")).toBeFalsy()
    })

    it("move down is invisible", async () => {
        const { queryByText } = render(
            <UnderlineMenu
                deleteFunction={() => ({})}
                moveFunction={() => ({})}
                id={1}
                listLength={2}
            />
        )
        expect(queryByText("Move Down")).toBeFalsy()
    })

    
    it("move up and down is visible", async () => {
        const { getByText } = render(
            <UnderlineMenu
                deleteFunction={() => ({})}
                moveFunction={() => ({})}
                id={1}
                listLength={3}
            />
        )
        expect(getByText("Move Up").hidden).toBeFalsy()
        expect(getByText("Move Down").hidden).toBeFalsy()
    })

    it("to have clickable elements", async () => {
        const { getByText } = render(
            <UnderlineMenu
                deleteFunction={mockDelete}
                moveFunction={mockMove}
                id={1}
                listLength={3}
            />
        )
        fireEvent.click(getByText("Delete"))
        expect(mockDelete.mock.calls.length).toBe(1)

        fireEvent.click(getByText("Move Up"))
        expect(mockMove.mock.calls.length).toBe(1)

        fireEvent.click(getByText("Move Down"))
        expect(mockMove.mock.calls.length).toBe(2)
    })
})