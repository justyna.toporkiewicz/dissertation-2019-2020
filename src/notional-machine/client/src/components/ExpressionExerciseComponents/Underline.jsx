import React from 'react'
import PropTypes from 'prop-types'
import UnderlineMenu from './UnderlineMenu'
import * as Constants from '../Constants'
import './Underline.css'

function getTextWidth(textLength, fontSize="16px") {
  if(textLength == 0) return 1
  let canvas = document.createElement("canvas")
  let context = canvas.getContext("2d")
  context.font = fontSize+" Courier New"
  let width = context.measureText(".".repeat(textLength)).width
  return Math.ceil(width)
}

function getHeight(index) {
  let element = document.getElementById(index)
  return element.scrollHeight + 3
}

function colorPicker (status) {
  switch(status) {
    case Constants.PART_1:
    case Constants.MISSING:
      return "inProgress"
    case Constants.CORRECT:
    case Constants.PART_2:
    case Constants.PART_2_SOLUTION:
      return "correct"
    case Constants.WRONG:
      return "wrong"
  }
}

class Underline extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notUnderlinedWidth: getTextWidth(this.props.startIndex),
      childrenWidth: getTextWidth(this.props.endIndex - this.props.startIndex),
      color: colorPicker(this.props.status),
      labelColor: colorPicker(this.props.labelStatus),
      textColor: colorPicker(this.props.evaluationCorrect),
      showMenu: false,
      explanation: this.showExplanation(this.props.part1solution, this.props.status, this.props.labelStatus),
      showHover: false,
      value: [],
      height: "20px"
    }
    this.openMenu = this.openMenu.bind(this)
    this.closeMenu = this.closeMenu.bind(this)
    this.handleMouseHover = this.handleMouseHover.bind(this)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({
      notUnderlinedWidth: getTextWidth(nextProps.startIndex),
      childrenWidth: getTextWidth(nextProps.endIndex - nextProps.startIndex),
      color: colorPicker(nextProps.status)
    })
  }

  openMenu(event) {
    event.preventDefault();
    
    this.setState({ showMenu: true }, () => {
      document.addEventListener('click', this.closeMenu);
    });
  }
  
  closeMenu() {
    this.setState({ showMenu: false }, () => {
      document.removeEventListener('click', this.closeMenu);
    });
  }

  handleMouseHover() {
    this.setState({showHover: !this.state.showHover})
  }

  showExplanation(user, underlineStatus, labelStatus) {
    if (user == Constants.TEACHER) {
      if (underlineStatus == Constants.CORRECT && labelStatus == Constants.CORRECT) {
        return Constants.TEACHER_ALL_GREEN
      }
      else if (underlineStatus == Constants.CORRECT) {
        return Constants.TEACHER_GREEN_UNDERLINE_YELLOW_LABEL
      }
      else {
        return Constants.TEACHER_ALL_YELLOW
      }
    }
    else {
      if (underlineStatus == Constants.CORRECT && labelStatus == Constants.CORRECT) {
        return Constants.STUDENT_ALL_GREEN
      }
      else if (underlineStatus == Constants.CORRECT) {
        return Constants.STUDENT_GREEN_UNDERLINE_RED_LABEL
      }
      else {
        return Constants.STUDENT_ALL_RED
      }
    }
  }

  updateLocalValue(event){
    if(event.target.value+"" == "") {
      this.setState({
        height: "20px"
      })
    }
    else {this.setState({
      height: getHeight(this.props.index)+"px"
    })}
    event.preventDefault();

    this.setState({
      value: [event.target.value+""]
    });
    this.props.updateInput(event);
  }

  render() {
    return(
      <div 
        onMouseEnter={this.props.part1solution != "false" ?
          this.handleMouseHover :
          () => ({})}
        onMouseLeave={this.props.part1solution != "false" ?
          this.handleMouseHover :
          () => ({})}
      >
        {/*displacement before underline*/}
        <span style={{marginRight: this.state.notUnderlinedWidth+2 + 'px'}} />

        {/*underline main body*/}
        <span 
          onClick={this.props.status == Constants.PART_1 ?
            this.openMenu :
            ()=>({})} 
          className={"line " + this.state.color + (this.props.status == Constants.PART_1 ?
            " pointer" :
            "")} 
          style={
            this.props.status == Constants.PART_2 || this.props.status == Constants.PART_2_SOLUTION || sessionStorage.getItem(Constants.MODE) == Constants.TEACHER_STUDENT_SOLUTION ?
              null :
              {paddingRight: this.state.childrenWidth + 'px'}}
          aria-label={"underline"+(this.props.index+1)}>

            {/*part 2 input box*/}
            {this.props.status == Constants.PART_2 ? 
                <textarea
                  className="onLineText"
                  type="text"
                  id={this.props.index}
                  name={"line"+this.props.index}
                  value={this.state.value}
                  style={{width: this.state.childrenWidth+1 + 'px', fontFamily: "Courier New, Courier, monospace", lineHeight: '11px', resize: "none", height: this.state.height}}
                  onChange={this.updateLocalValue.bind(this)}
                  aria-label={"underlineInput"+(this.props.index+1)} />
              : 
              null}

            {/*static text*/}
            {this.props.status == Constants.PART_2_SOLUTION || sessionStorage.getItem(Constants.MODE) == Constants.TEACHER_STUDENT_SOLUTION ?
              <span
                className={this.state.textColor + " onLineText"}
                style={{
                  display: 'inline-block',
                  width: this.state.childrenWidth + 'px'}}
                >
                {this.props.evaluated}
              </span>
              : null
            }
        </span>

        <span className={"label "+ this.state.labelColor} >{this.props.index+1}</span>

        {/*menu to move lines in part 1*/}
        {this.state.showMenu ? 
          <UnderlineMenu
            deleteFunction={this.props.deleteFunction}
            moveFunction={this.props.moveFunction}
            id={this.props.index}
            listLength={this.props.listLength}
          /> : null
        }
        
        {/*hover explanation*/}
        {this.props.part1solution != "false" && this.state.showHover ?
          <div>
            <span style={{marginRight: this.state.notUnderlinedWidth+2 + 'px', maxHeight: 0+'px'}} />
            <span className="hoverExplanation">
              {this.state.explanation}
            </span>
          </div>
          : null
        }
      </div>
    )
  }
}


Underline.defaultProps = {
  listLength: 0,
  status: Constants.MISSING,
  labelStatus: Constants.MISSING,
  part1solution: "false",
  evaluated: "",
  evaluationCorrect: Constants.WRONG,
  deleteFunction: () => ({}),
  moveFunction: () => ({}),
  updateInput: () => ({}),
} 

Underline.propTypes = {
  startIndex: PropTypes.number,
  endIndex: PropTypes.number,
  index: PropTypes.number,
  evaluated: PropTypes.string,
  evaluationCorrect: PropTypes.string,
  deleteFunction: PropTypes.func,
  moveFunction: PropTypes.func,
  updateInput: PropTypes.func,
  listLength: PropTypes.number,
  status: PropTypes.string,
  labelStatus: PropTypes.string,
  part1solution: PropTypes.string,
}

export default Underline