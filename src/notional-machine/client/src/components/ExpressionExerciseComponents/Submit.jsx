import React from 'react'
import PropTypes from 'prop-types'
import './Submit.css'

const Submit = ({value}) => (
    <div className="buttonLayout">
        <input className="btn btn-success center" type="submit" value={value} />
    </div>
)

Submit.defaultProps = {
    value: "Submit",
}

Submit.propTypes = {
    value: PropTypes.string,
}

export default Submit