import React from 'react'
import PropTypes from 'prop-types'
import './ExpressionToken.css'

const ExpressionToken = ({content, index, underlineFunction, hoverChange, changeColor}) => (
    <span
      onClick={()=>underlineFunction(index)}
      className={"token " + (hoverChange ? "onHover" : "") + (changeColor ? " clickedColor" : "")}
    >
      {content}
    </span>
)

ExpressionToken.defaultProps = {
  underlineFunction: ()=>({}),
  hoverChange: false,
  changeColor: false,
}

ExpressionToken.propTypes = {
  content: PropTypes.string,
  index: PropTypes.number,
  underlineFunction: PropTypes.func,
  hoverChange: PropTypes.bool,
  changeColor: PropTypes.bool,
}

export default ExpressionToken