import React from 'react'
import PropTypes from 'prop-types'
import "./UnderlineMenu.css"

const UnderlineMenu = ({deleteFunction, id, moveFunction, listLength}) => {
    return(
        <div className="btn-group-vertical menu">
            <div
              onClick={()=>deleteFunction(id)}
              className="btn btn-primary">
                  Delete
            </div>
            {id != 0 ?
                <div
                    onClick={()=>moveFunction(id, true)}
                    className="btn btn-primary">
                        Move Up
                </div>:
            null}

            {id != (listLength-1) ? 
                <div
                    onClick={()=>moveFunction(id, false)}
                    className="btn btn-primary">
                        Move Down
                </div> : 
            null}
            
        </div>
    )
}

UnderlineMenu.propTypes = {
    deleteFunction: PropTypes.func,
    moveFunction: PropTypes.func,
    id: PropTypes.number,
    listLength: PropTypes.number,
}

export default UnderlineMenu
