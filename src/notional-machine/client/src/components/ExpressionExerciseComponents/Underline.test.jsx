import React from 'react'
import { render, fireEvent, getByText } from '@testing-library/react'
import Underline from './Underline'
import * as Constant from '../Constants'
import 'jest-canvas-mock'

describe("Underline", () => {
    it("renders correctly", async () => {
        const { getByText, queryByText } = render(
            <Underline 
                startIndex={1}
                endIndex={5}
                index={0}
                listLength={1}
            />)
        expect(getByText("1")).toBeTruthy()

        fireEvent.mouseEnter(getByText("1"))
        expect(queryByText(Constant.STUDENT_ALL_RED)).toBeFalsy()
        fireEvent.mouseLeave(getByText("1"))
        expect(queryByText(Constant.STUDENT_ALL_RED)).toBeFalsy()
    })

    it("shows hover in solution part 1", async () => {
        const { getByText, queryByText, getByLabelText } = render(
            <Underline 
                startIndex={1}
                endIndex={5}
                index={0}
                listLength={1}
                part1solution={Constant.STUDENT}
            />)
        fireEvent.mouseEnter(getByText("1"))
        expect(getByText(Constant.STUDENT_ALL_RED)).toBeTruthy()

        fireEvent.mouseLeave(getByText("1"))
        expect(queryByText(Constant.STUDENT_ALL_RED)).toBeFalsy()

        fireEvent.click(getByLabelText("underline1"))
        expect(queryByText("Move Up")).toBeFalsy()
    })

    it("shows the menu when clicked on underline in exercise part 1", async() => {
        const { getByText, getByLabelText, queryByText } = render(
            <Underline 
                startIndex={1}
                endIndex={5}
                index={1}
                listLength={3}
                status={Constant.PART_1}
        />)
        fireEvent.click(getByLabelText("underline2"))
        expect(getByText("Move Up")).toBeTruthy()

        fireEvent.click(getByText("Move Up"))
        expect(queryByText("Move Up")).toBeFalsy()

        fireEvent.click(getByLabelText("underline2"))
        expect(getByText("Delete")).toBeTruthy()

        fireEvent.click(getByText("Delete"))
        expect(queryByText("Delete")).toBeFalsy()
    })

    it("has an input box when in expercise part 2", async () => {
        const { getByLabelText } = render(
            <Underline 
                startIndex={1}
                endIndex={5}
                index={1}
                listLength={3}
                status={Constant.PART_2}
        />)
        expect(fireEvent.change(getByLabelText('underlineInput2'), {target: {value: 'underline-value'}})).toBeTruthy()
        expect(fireEvent.change(getByLabelText('underlineInput2'), {target: {value: ''}})).toBeTruthy()
    })

    it("shows the value when in exercise part 2 solution", async () => {
        const { getByText } = render(
            <Underline
                startIndex={1}
                endIndex={5}
                index={1}
                listLength={3}
                status={Constant.PART_2_SOLUTION}
                evaluated="4"
            />)
        expect(getByText("4")).toBeTruthy()
    })

    it("shows appropriate explanation in solution part 1", async () => {
        const teacher_correct = render(
            <Underline 
                startIndex={1}
                endIndex={5}
                index={0}
                listLength={1}
                part1solution={Constant.TEACHER}
                status={Constant.CORRECT}
                labelStatus={Constant.CORRECT}
            />)
        fireEvent.mouseEnter(teacher_correct.getByText("1"))
        expect(teacher_correct.getByText(Constant.TEACHER_ALL_GREEN)).toBeTruthy()
        const teacher_partially_correct = render(
                <Underline 
                    startIndex={1}
                    endIndex={5}
                    index={1}
                    listLength={1}
                    part1solution={Constant.TEACHER}
                    status={Constant.CORRECT}
            />)
        fireEvent.mouseEnter(teacher_partially_correct.getByText("2"))
        expect(teacher_partially_correct.getByText(Constant.TEACHER_GREEN_UNDERLINE_YELLOW_LABEL)).toBeTruthy()

        const teacher_missing = render(
                <Underline 
                    startIndex={1}
                    endIndex={5}
                    index={2}
                    listLength={1}
                    part1solution={Constant.TEACHER}
            />)
        fireEvent.mouseEnter(teacher_missing.getByText("3"))
        expect(teacher_missing.getByText(Constant.TEACHER_ALL_YELLOW)).toBeTruthy()
    
        const student_correct = render(
                <Underline 
                    startIndex={1}
                    endIndex={5}
                    index={3}
                    listLength={1}
                    part1solution={Constant.STUDENT}
                    status={Constant.CORRECT}
                    labelStatus={Constant.CORRECT}
            />)
        fireEvent.mouseEnter(student_correct.getByText("4"))
        expect(student_correct.getByText(Constant.STUDENT_ALL_GREEN)).toBeTruthy()
    
        const student_partially_correct = render(
                <Underline 
                    startIndex={1}
                    endIndex={5}
                    index={4}
                    listLength={1}
                    part1solution={Constant.STUDENT}
                    status={Constant.CORRECT}
            />)
        fireEvent.mouseEnter(student_partially_correct.getByText("5"))
        expect(student_partially_correct.getByText(Constant.STUDENT_GREEN_UNDERLINE_RED_LABEL)).toBeTruthy()
    })
})