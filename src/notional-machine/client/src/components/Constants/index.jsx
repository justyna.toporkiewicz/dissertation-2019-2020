export const MODE = 'status'
export const EXPRESSION_EVALUATION = 'Expression_Evaluation'
export const PROGRAM_FLOW = 'Program_Flow'

// exercise display mode
export const TEACHER_CREATE = 'TEACHER-CREATE'
export const TEACHER_UPDATE = 'TEACHER-UPDATE'
export const TEACHER_VIEW = 'TEACHER-VIEW'
export const TEACHER_STUDENT_SOLUTION = 'TEACHER_STUDENT_SOLUTION'
export const TEACHER = 'TEACHER'
export const STUDENT = 'STUDENT'

// exercise section
export const PART_1 = "PART_1"
export const PART_1_SOLUTION = "PART_1_SOLUTION"
export const PART_2 = "PART_2"
export const PART_2_SOLUTION = "PART_2_SOLUTION"

// underline/underline value status
export const MISSING = "MISSING"
export const CORRECT = "CORRECT"
export const WRONG = "WRONG"

// solution explanations
// correct side
export const TEACHER_ALL_GREEN = "All green: This correct underline appears in your solution in the correct sequence"
export const TEACHER_GREEN_UNDERLINE_YELLOW_LABEL = "Green underline, yellow label: This underline appears in your solution, but not in the correct sequence"
export const TEACHER_ALL_YELLOW = "All yellow: This underline does not appear in your solution at all"
//student side
export const STUDENT_ALL_GREEN = "All green: This underline matches exactly to the correct solution"
export const STUDENT_GREEN_UNDERLINE_RED_LABEL = "Green underline, red label: This underline matches to an underline in the correct solution but is not in the correct sequence"
export const STUDENT_ALL_RED = "All red: This underline does not match any underline in the correct solution"