# Plan

* Helping Students practice expression evaluation (ID: 8753)
* Justyna Toporkiewicz
* 2270645T
* Quintin Cutts

Week-by-week plan for the whole project.

## Winter semester

* **Week 1** Setup, research and planning
* **Week 2** Preparing the baseline system
* **Week 3** Planning the structure of components
* **Week 4** Starting the implementation of exercise specific elements
* **Week 5** Working on the main component of exercise
* **Week 6** Working on the main component of exercise
* **Week 7** Creating the layouts of the exercise pages
* **Week 8** Create feedback pages
* **Week 9** Working on the structure of data to be saved
* **Week 10** Updating the database
* **Week 11 [PROJECT WEEK]** Connecting to the database
* **Week 12 [PROJECT WEEK]** Status report submitted
* **Week 13** Addressing supervisor comments
* **Week 14** Starting dissertation

## Winter break

## Spring Semester

* **Week 15** Writing dissertation + Addressing supervisor comments
* **Week 16** Writing dissertation
* **Week 17** Refactoring code
* **Week 18** Writing tests
* **Week 19** Writing tests + preparation for AB testing
* **Week 20** Addressing supervisor comments + Writing dissertation
* **Week 21** Addressing supervisor comments
* **Week 22** Writing dissertation
* **Week 23** Small stylistic changes + Writing dissertation
* **Week 24** AB Evaluation
* **Week 25** AB Evaluation + Usability testing
* **Week 26 [TERM ENDS]** Writing dissertation + Usability testing
* **Week 27** Writing dissertation + small stylistic changes
* **Week 28** Writing dissertation + preparing presentation
* **Week 29** Dissertation submission deadline and presentations
